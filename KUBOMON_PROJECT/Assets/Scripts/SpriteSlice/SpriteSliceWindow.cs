﻿//  SpriteSliceWindow.cs

//  http://kan-kikuchi.hatenablog.com/entry/SpriteSliceWindow

//

//  Created by kan.kikuchi on 2016.09.22.



using UnityEditor;

using UnityEngine;

using System.Collections;

using System.Collections.Generic;



/// <summary>

/// スプライトを分割する用のウィンドウ

/// </summary>

public class SpriteSliceWindow : EditorWindow
{



    //現在選択中のテクスチャ

    private Texture2D _texture = null;



    //分割する時の各スプライトのサイズ

    private int _width = 16, _height = 16;



    //=================================================================================

    //ウィンドウ表示

    //=================================================================================



    //メニューからウィンドウを表示

    [MenuItem("Tools/Open/SpriteSliceWindow")]

    public static void Open()
    {

        //ウィンドウを生成し、最前面に表示

        CreateInstance<SpriteSliceWindow>().ShowUtility();

    }



    //=================================================================================

    //画像分割

    //=================================================================================



    //スブライトをスライスする

    private void ToSliceSprite()
    {

        if (_texture == null)
        {

            return;

        }



        //対象のテクスチャのパスを取得、そこからインポーターを生成

        string texturePath = AssetDatabase.GetAssetPath(_texture);

        TextureImporter textureImporter = AssetImporter.GetAtPath(texturePath) as TextureImporter;



        //Multipleにして分割できるように(すでにMultipleの状態からだとMetaDataが変更できなかったので、一旦Singleに)

        textureImporter.spriteImportMode = SpriteImportMode.Single;

        textureImporter.spriteImportMode = SpriteImportMode.Multiple;



        //分割した各スプライトごとにSpriteMetaData作成

        List<SpriteMetaData> metaDataList = new List<SpriteMetaData>();



        for (int y = _texture.height; y > 0; y -= _height)
        {

            for (int x = 0; x < _texture.width; x += _width)
            {



                //はみ出る場合は作成しない

                if (x + _width > _texture.width || y - _height < 0)
                {

                    continue;

                }



                SpriteMetaData smd = new SpriteMetaData();

                smd.pivot = new Vector2(0.5f, 0.5f);

                smd.alignment = (int)SpriteAlignment.Custom;

                smd.name = _texture.name + "_" + metaDataList.Count.ToString();

                smd.rect = new Rect(x, y - _height, _width, _height);



                metaDataList.Add(smd);

            }

        }



        textureImporter.spritesheet = metaDataList.ToArray();

        AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);

    }



    //=================================================================================

    //表示するGUIの設定

    //=================================================================================



    private void OnGUI()
    {



        //設定されている画像のパスを表示

        EditorGUILayout.BeginVertical(GUI.skin.box);



        EditorGUILayout.BeginVertical(GUI.skin.box);

        if (_texture != null)
        {

            EditorGUILayout.LabelField("TexturePath : " + _texture.name);

        }

        EditorGUILayout.EndVertical();



        //設定されている分割サイズを表示

        EditorGUILayout.BeginVertical(GUI.skin.box);

        _width = EditorGUILayout.IntField("Sprite Width", _width);

        _height = EditorGUILayout.IntField("Sprite Height", _height);

        EditorGUILayout.EndVertical();



        EditorGUILayout.EndVertical();



        GUILayout.Space(10);



        //D&D出来るGUIを作成、 ドロップされたテクスチャを取得

        Texture2D texture = CreateDragAndDropGUI();

        if (texture != null)
        {

            _texture = texture;

        }



        //スブライトのスライスを実行するボタン

        if (GUILayout.Button("ToSliceSprite"))
        {

            ToSliceSprite();

        }

    }



    //D&DのGUIを作成

    private Texture2D CreateDragAndDropGUI()
    {



        //D&D出来る場所を描画

        Rect dropArea = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));

        GUI.Box(dropArea, "\nDrag & Drop Area\n");



        //マウスの位置がD&Dの範囲になければスルー

        if (!dropArea.Contains(Event.current.mousePosition))
        {

            return null;

        }



        //現在のイベントを取得

        EventType eventType = Event.current.type;



        //ドラッグ＆ドロップで操作が 更新されたとき or 実行したとき

        List<Object> list = new List<Object>();



        if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
        {

            //カーソルに+のアイコンを表示

            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;



            //ドロップされたオブジェクトをリストに登録

            if (eventType == EventType.DragPerform)
            {

                list = new List<Object>(DragAndDrop.objectReferences);



                //ドラッグを受け付ける(ドラッグしてカーソルにくっ付いてたオブジェクトが戻らなくなる)

                DragAndDrop.AcceptDrag();

            }



            //イベントを使用済みにする

            Event.current.Use();

        }



        if (list.Count == 0)
        {

            return null;

        }



        return list[0] as Texture2D;

    }





}