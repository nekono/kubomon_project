﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Systems.Kubomon {
    /// <summary>
    /// データの取り出しとかやるクラス
    /// </summary>
    public static class KuboListManager {
        private static KuboList kuboData;

        static KuboListManager() {
            kuboData = (KuboList) Resources.Load("Datas/KuboList");
        }

        public static KuboData GetKuboData(int id) {
            if(id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id);
            return new KuboData();
        }

        public static KuboImg GetKuboImg(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).KuboImg;
            return new KuboImg();
        }

        public static BaseStats GetKuboBaseStats(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats;
            return new BaseStats();
        }

        public static int GetKuboHP(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats.H;
            return 0;
        }
        
        public static int GetKuboAttack(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats.A;
            return 0;
        }
        public static int GetKuboDefense(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats.B;
            return 0;
        }
        public static int GetKuboSpAttack(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats.C;
            return 0;
        }
        public static int GetKuboSpDefense(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats.D;
            return 0;
        }
        public static int GetKuboSpeed(int id) {
            if (id != -1) return kuboData.Datas.FirstOrDefault(i => i.UniqueID == id).BaseStats.S;
            return 0;
        }

        public static EType[] GetKuboType(int id) {
            EType[] types = new EType[2];
            if (id != -1) {
                var kubo = kuboData.Datas.FirstOrDefault(i => i.UniqueID == id);
                types[0] = kubo.Type1;
                types[1] = kubo.Type2;
            }

            return types;
        }  

        public static List<KuboData> GetAllKuboData() {
            return kuboData.Datas;
        }

        public static void UpdateKuboData(List<KuboData> datas, bool with_img = false) {
            if (kuboData == null) {
                kuboData = Resources.Load("Datas/KuboList", typeof(KuboList)) as KuboList;
                if (kuboData == null) {
                    Debug.Log(Resources.Load("Datas/KuboList", typeof(KuboList)) as KuboList);
                }
            }

            if (kuboData.Datas == null) {
                kuboData.Datas = ((KuboList) Resources.Load("Datas/KuboList")).Datas;
            }

            var local = new List<KuboData>(datas);
            if (!with_img) {
                local = local.Select(i => {
                        if (kuboData.Datas.Any(o => o.UniqueID == i.UniqueID)) {
                            i.KuboImg = new KuboImg(kuboData.Datas.First(o => o.UniqueID == i.UniqueID).KuboImg);
                        }

                        return i;
                    })
                    .ToList();
            }

            kuboData.Datas = local;
        }
    }
}
