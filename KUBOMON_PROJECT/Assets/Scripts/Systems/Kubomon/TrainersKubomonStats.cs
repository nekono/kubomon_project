using System;
using Systems.Move;
using UnityEngine;

namespace Systems.Kubomon {
    [Serializable]
    public class TrainersKubomonStats : KubomonStats {
        [SerializeField] protected string nickName;
        [SerializeField] protected int glassesNumber;
        [SerializeField] protected int happiness;//なつき度

        public string NickName {
            get {
                if (!string.IsNullOrEmpty(nickName)) {
                    return nickName;
                }

                return KuboListManager.GetKuboData(KubomonId).Name;
            }
            set { nickName = value; }
        }

        public int GlassesNumber => glassesNumber;
        public EffortValues EffortValues; //努力値
        public int Happiness => happiness;
        
        public override int HP => StatsCalculator.HitPoint(kubomonId, level, individualValues.H, EffortValues.H);
        public override int Attack => StatsCalculator.Attack(kubomonId, level, individualValues.A, nature, EffortValues.A);
        public override int Defense => StatsCalculator.Defense(kubomonId, level, individualValues.B, nature, EffortValues.B);
        public override int SpAttack => StatsCalculator.SpAttack(kubomonId, level, individualValues.C, nature, EffortValues.C);
        public override int SpDefense => StatsCalculator.SpDefense(kubomonId, level, individualValues.D, nature, EffortValues.D);
        public override int Speed => StatsCalculator.Speed(kubomonId, level, individualValues.S, nature, EffortValues.S);

        public TrainersKubomonStats(int kubomonId = 0, int level = 0, int abilityNumber = 0, EGender gender = EGender.Male, ENature nature = ENature.Hardy, IndividualValues individualValues = default(IndividualValues), int damage = 0, MoveSet moveSet = null, int heldItemNumber = -1, EStatusAilment statusAilment = EStatusAilment.None, string nickName = null, int glassesNumber = 0, int happiness = 0, EffortValues effortValues = default(EffortValues)) : base(kubomonId, level, abilityNumber, gender, nature, individualValues, damage, moveSet, heldItemNumber, statusAilment) {
            this.nickName = nickName;
            this.glassesNumber = glassesNumber;
            this.happiness = happiness;
            EffortValues = effortValues;
        }
        
        public TrainersKubomonStats(KubomonStats kubomonStats = null, string nickName = null, int glassesNumber = 0, int happiness = 0, EffortValues effortValues = default(EffortValues)) : base(kubomonStats) {
            this.nickName = nickName;
            this.glassesNumber = glassesNumber;
            this.happiness = happiness;
            EffortValues = effortValues;
        }

        public TrainersKubomonStats(TrainersKubomonStats trainersKubomonStats) : base(trainersKubomonStats) {
            nickName = trainersKubomonStats.nickName;
            glassesNumber = trainersKubomonStats.glassesNumber;
            happiness = trainersKubomonStats.happiness;
            EffortValues = trainersKubomonStats.EffortValues;
        }
    }
}
