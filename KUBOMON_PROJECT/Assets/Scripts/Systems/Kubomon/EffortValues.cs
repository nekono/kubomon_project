using UnityEngine;

namespace Systems.Kubomon {
    /// <summary>
    /// クボモンの努力値
    /// </summary>
    public struct EffortValues {
        private int h, a, b, c, d, s;
        public int H { get { return h; } set { h = Mathf.Clamp(value, 0, 252); } }
        public int A { get { return a; } set { a = Mathf.Clamp(value, 0, 252); } }
        public int B { get { return b; } set { b = Mathf.Clamp(value, 0, 252); } }
        public int C { get { return c; } set { c = Mathf.Clamp(value, 0, 252); } }
        public int D { get { return d; } set { d = Mathf.Clamp(value, 0, 252); } }
        public int S { get { return s; } set { s = Mathf.Clamp(value, 0, 252); } }
    }
}
