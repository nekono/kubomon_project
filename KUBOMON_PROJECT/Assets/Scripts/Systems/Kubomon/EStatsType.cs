namespace Systems.Kubomon {
    public enum EStatsType {
        HitPoints, Attack, Defence,
        SpAttack, SpDefence, Speed,
        Evasion, Accuracy
    }
}
