﻿using System;

namespace Systems.Kubomon {
    /// <summary>
    /// クボモン一種類ごとのデータ
    /// </summary>
    [Serializable]
    public struct KuboData {
        public string Name; //クボモンの名前。図鑑で表示されるやつ
        public string FormName; //フォルム名。図鑑で小さく表示されるやつ
        public int UniqueID; //固有ID。≠図鑑番号
        public int DexID; //図鑑番号
        public EType Type1; //タイプ1
        public EType Type2; //タイプ2
        public BaseStats BaseStats; //種族値
        public string AbilityName1; //特性名
        public string AbilityName2; //特性名
        public string AbilityName3; //特性名
        public KuboImg KuboImg;

        public KuboData(string name = "New Kubomon", string formName = "", int id = -1, int dexId = -1, EType type1 = EType.None, EType type2 = EType.None, BaseStats baseStats = new BaseStats(), string abilityName1 = "", string abilityName2 = "", string abilityName3 = "", KuboImg kuboImg = new KuboImg()) {
            Name = name;
            FormName = formName;
            UniqueID = id;
            DexID = dexId;
            Type1 = type1;
            Type2 = type2;
            BaseStats = baseStats;
            AbilityName1 = abilityName1;
            AbilityName2 = abilityName2;
            AbilityName3 = abilityName3;
            KuboImg = kuboImg;
        }
    }
}