﻿namespace Systems.Kubomon {
    public enum EExperienceType {
        Erratic, Fast, MediumFast, MediumSlow, Slow, Fluctuating
    }
}
