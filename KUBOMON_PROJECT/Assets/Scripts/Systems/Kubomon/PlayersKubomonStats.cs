﻿using System;
using Systems.Move;
using UnityEngine;

namespace Systems.Kubomon {
    [Serializable]
    public class PlayersKubomonStats : TrainersKubomonStats {
        public readonly string TrainerName;
        public readonly int TrainerID;
        [SerializeField] private int toNextLevel;
        public int ToNextLevel => toNextLevel;

        public PlayersKubomonStats(int kubomonId = 0, int level = 0, int abilityNumber = 0, EGender gender = EGender.Male, ENature nature = ENature.Hardy, IndividualValues individualValues = default(IndividualValues), int damage = 0, MoveSet moveSet = null, int heldItemNumber = -1, EStatusAilment statusAilment = EStatusAilment.None, string nickName = null, int glassesNumber = 0, int happiness = 0, EffortValues effortValues = default(EffortValues), string trainerName = null, int trainerId = 0, int nextLevel = 0) : base(kubomonId, level, abilityNumber, gender, nature, individualValues, damage, moveSet, heldItemNumber, statusAilment, nickName, glassesNumber, happiness, effortValues) {
            TrainerName = trainerName;
            TrainerID = trainerId;
            toNextLevel = nextLevel;
        }
        public PlayersKubomonStats(KubomonStats kubomonStats = null, string nickName = null, int glassesNumber = 0, int happiness = 0, EffortValues effortValues = default(EffortValues), string trainerName = null, int trainerId = 0, int nextLevel = 0) : base(kubomonStats, nickName, glassesNumber, happiness, effortValues) {
            TrainerName = trainerName;
            TrainerID = trainerId;
            toNextLevel = nextLevel;
        }
        public PlayersKubomonStats(KubomonStats kubomonStats = null, string nickName = null, int glassesNumber = 0, int happiness = 0, string trainerName = null, int trainerId = 0, int nextLevel = 0) : base(kubomonStats, nickName, glassesNumber, happiness) {
            TrainerName = trainerName;
            TrainerID = trainerId;
            toNextLevel = nextLevel;
        }
    }
}
