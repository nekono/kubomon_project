﻿using System.Collections.Generic;
using UnityEngine;

namespace Systems.Kubomon {
    /// <summary>
    /// 実際にクボモンのデータが入ってるクラス
    /// </summary>
    [CreateAssetMenu(menuName = "Resources/Create Kubo")]
    public class KuboList : ScriptableObject {
        [SerializeField] public List<KuboData> Datas;
    }
}