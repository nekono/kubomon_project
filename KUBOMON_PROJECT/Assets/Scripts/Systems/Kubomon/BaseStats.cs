﻿using System;
using UnityEngine;

namespace Systems.Kubomon {
    /// <summary>
    /// クボモンの種族値
    /// </summary>
    [Serializable]
    public struct BaseStats {
        [Range(0, 255)] public int H, A, B, C, D, S;

        public BaseStats(int h = 0, int a = 0, int b = 0, int c = 0, int d = 0, int s = 0) {
            H = h;
            A = a;
            B = b;
            C = c;
            D = d;
            S = s;
        }
    }
}