﻿using System;

namespace Systems.Kubomon {
    /// <summary>
    /// ステータス計算機
    /// </summary>
    public static class StatsCalculator {
        /// <summary>
        /// 次レベルまでに必要な経験値を返す
        /// </summary>
        /// <param name="type">経験値タイプ</param>
        /// <param name="nextLevel">次のレベル</param>
        /// <returns>必要経験値</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static int SetNextExp(EExperienceType type, int nextLevel) {
            var temp = nextLevel * nextLevel * nextLevel;
            switch (type) {
                case EExperienceType.Erratic:
                    if (nextLevel <= 50) {
                        return temp * (100 - nextLevel) / 50;
                    } else if (nextLevel <= 68) {
                        return temp * (150 - nextLevel) / 100;
                    } else if (nextLevel <= 98) {
                        return temp * (637 - 10 * nextLevel / 3) / 500;
                    } else {
                        return temp * (160 - nextLevel) / 100;
                    }
                case EExperienceType.Fast:
                    return temp * 4 / 5;
                case EExperienceType.MediumFast:
                    return temp;
                case EExperienceType.MediumSlow:
                    return 6 * temp / 5 - 15 * nextLevel * nextLevel + 100 * nextLevel - 140;
                case EExperienceType.Slow:
                    return temp * 5 / 4;
                case EExperienceType.Fluctuating:
                    if (nextLevel <= 15) {
                        return temp * (24 + (nextLevel + 1) / 3) / 50;
                    } else if (nextLevel <= 36) {
                        return temp * (14 + nextLevel) / 50;
                    } else {
                        return temp * (32 + (nextLevel / 2) / 50);
                    }
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public static int HitPoint(int id, int level, int individualValue, int effortValue = 0) {
            return (KuboListManager.GetKuboHP(id) * 2 + individualValue + effortValue / 4) * level / 100 + level + 10;
        }

        public static int Attack(int id, int level, int individualValue, ENature nature, int effortValue = 0) {
            switch (nature) {
                case ENature.Lonely:
                case ENature.Brave:
                case ENature.Adamant:
                case ENature.Naughty:
                    return ((KuboListManager.GetKuboAttack(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 11 / 10;
                case ENature.Bold:
                case ENature.Timid:
                case ENature.Modest:
                case ENature.Calm:
                    return ((KuboListManager.GetKuboAttack(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 9 / 10;
                default:
                    return ((KuboListManager.GetKuboAttack(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5);
            }
        }

        public static int Defense(int id, int level, int individualValue, ENature nature, int effortValue = 0) {
            switch (nature) {
                case ENature.Bold:
                case ENature.Relaxed:
                case ENature.Impish:
                case ENature.Lax:
                    return ((KuboListManager.GetKuboDefense(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 11 / 10;
                case ENature.Lonely:
                case ENature.Hasty:
                case ENature.Mild:
                case ENature.Gentle:
                    return ((KuboListManager.GetKuboDefense(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 9 / 10;
                default:
                    return ((KuboListManager.GetKuboDefense(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5);
            }
        }

        public static int SpAttack(int id, int level, int individualValue, ENature nature, int effortValue = 0) {
            switch (nature) {
                case ENature.Modest:
                case ENature.Mild:
                case ENature.Quiet:
                case ENature.Rash:
                    return ((KuboListManager.GetKuboSpAttack(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 11 / 10;
                case ENature.Adamant:
                case ENature.Impish:
                case ENature.Careful:
                case ENature.Jolly:
                    return ((KuboListManager.GetKuboSpAttack(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 9 / 10;
                default:
                    return ((KuboListManager.GetKuboSpAttack(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5);
            }
        }

        public static int SpDefense(int id, int level, int individualValue, ENature nature, int effortValue = 0) {
            switch (nature) {
                case ENature.Calm:
                case ENature.Gentle:
                case ENature.Sassy:
                case ENature.Careful:
                    return ((KuboListManager.GetKuboSpDefense(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 11 / 10;
                case ENature.Naughty:
                case ENature.Lax:
                case ENature.Rash:
                case ENature.Naive:
                    return ((KuboListManager.GetKuboSpDefense(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 9 / 10;
                default:
                    return ((KuboListManager.GetKuboSpDefense(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5);
            }
        }

        public static int Speed(int id, int level, int individualValue, ENature nature, int effortValue = 0) {
            switch (nature) {
                case ENature.Timid:
                case ENature.Hasty:
                case ENature.Jolly:
                case ENature.Naive:
                    return ((KuboListManager.GetKuboSpeed(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 11 / 10;
                case ENature.Brave:
                case ENature.Relaxed:
                case ENature.Quiet:
                case ENature.Sassy:
                    return ((KuboListManager.GetKuboSpeed(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5) * 9 / 10;
                default:
                    return ((KuboListManager.GetKuboSpeed(id) * 2 + individualValue + effortValue / 4) * level / 100 + 5);
            }
        }
    }
}
