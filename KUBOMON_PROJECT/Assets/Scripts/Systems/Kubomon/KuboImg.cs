﻿using System;
using UnityEngine;

namespace Systems.Kubomon {
    /// <summary>
    /// クボモンのイラスト
    /// </summary>
    [Serializable]
    public struct KuboImg {
        public Sprite Foward; //相手のクボモン。図鑑での表示。
        public Sprite Back; //自分
        public Sprite SDImage; //手持ち

        public KuboImg(Sprite f, Sprite b, Sprite sd) {
            Foward = f;
            Back = b;
            SDImage = sd;
        }

        public KuboImg(KuboImg kuboImg) {
            Foward = kuboImg.Foward;
            Back = kuboImg.Back;
            SDImage = kuboImg.SDImage;
        }
    }
}