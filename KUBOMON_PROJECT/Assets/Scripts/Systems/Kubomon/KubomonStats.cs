using System;
using Systems.Move;
using UnityEngine;

namespace Systems.Kubomon {
    /// <summary>
    /// クボモン一体ごとのデータ
    /// </summary>
    [Serializable]
    public class KubomonStats {
        [SerializeField] protected int kubomonId;
        [SerializeField] protected int level;
        [SerializeField] protected int abilityNumber; //特性
        [SerializeField] protected EGender gender; //性別
        [SerializeField] protected ENature nature; //性格
        [SerializeField] protected IndividualValues individualValues; //個体値
        [SerializeField] protected int damage;

        public int KubomonId => kubomonId;
        public int Level => level;
        public int AbilityNumber => abilityNumber;
        public EGender Gender => gender;
        public ENature Nature => nature;
        public IndividualValues IndividualValues => individualValues;
        public MoveSet MoveSet; //技
        public int HeldItemNumber; //持ち物
        public int Damage { private get { return damage; } set { damage = Mathf.Clamp(value, 0, HP); } } //受けてるダメージ
        public EStatusAilment StatusAilment; //状態異常

        public virtual int HP => StatsCalculator.HitPoint(kubomonId, level, individualValues.H);
        public virtual int Attack => StatsCalculator.Attack(kubomonId, level, individualValues.A, nature);
        public virtual int Defense => StatsCalculator.Defense(kubomonId, level, individualValues.B, nature);
        public virtual int SpAttack => StatsCalculator.SpAttack(kubomonId, level, individualValues.C, nature);
        public virtual int SpDefense => StatsCalculator.SpDefense(kubomonId, level, individualValues.D, nature);
        public virtual int Speed => StatsCalculator.Speed(kubomonId, level, individualValues.S, nature);

        public KubomonStats(int kubomonId = 0, int level = 0, int abilityNumber = 0, EGender gender = EGender.Male, ENature nature = ENature.Hardy, IndividualValues individualValues = default(IndividualValues), int damage = 0, MoveSet moveSet = null, int heldItemNumber = -1, EStatusAilment statusAilment = EStatusAilment.None) {
            this.kubomonId = kubomonId;
            this.level = level;
            this.abilityNumber = abilityNumber;
            this.gender = gender;
            this.nature = nature;
            this.individualValues = individualValues;
            MoveSet = moveSet;
            HeldItemNumber = heldItemNumber;
            Damage = damage;
            StatusAilment = statusAilment;
        }

        public KubomonStats(KubomonStats kubomonStats) {
            kubomonId = kubomonStats.KubomonId;
            level = kubomonStats.Level;
            abilityNumber = kubomonStats.AbilityNumber;
            gender = kubomonStats.Gender;
            nature = kubomonStats.Nature;
            individualValues = kubomonStats.IndividualValues;
            MoveSet = kubomonStats.MoveSet;
            HeldItemNumber = kubomonStats.HeldItemNumber;
            Damage = kubomonStats.Damage;
            StatusAilment = kubomonStats.StatusAilment;
        }
    }
}
