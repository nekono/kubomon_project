using System;
using UnityEngine;

namespace Systems {
    /// <summary>
    /// シングルトンクラスが継承すべきやつ
    /// DontDestroyOnLoadは継承先で
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {
        private static T instance;

        public static T Instance {
            get {
                if (instance == null) {
                    Type t = typeof(T);
                    instance = (T) FindObjectOfType(t);
                    if (instance == null) {
                        var obj = new GameObject(t.Name);
                        instance = obj.AddComponent<T>();
                    }
                }

                return instance;
            }
        }

        protected virtual void Awake() {
            if (instance == null) {
                instance = this as T;
                return;
            }else if (Instance == this) {
                return;
            }
            Destroy(gameObject);
        }
    }
}
