using System.Collections.Generic;
using Systems.Move;
using UnityEngine;

namespace Systems.Move {
    [CreateAssetMenu(menuName = "Resources/Create MoveList")]
    public class MoveList : ScriptableObject {
        [SerializeField] public List<MoveData> Moves;
    }
}
