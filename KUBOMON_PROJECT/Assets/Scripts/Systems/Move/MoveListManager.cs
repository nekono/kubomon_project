using System.Linq;
using UnityEngine;

namespace Systems.Move {
    public static class MoveListManager {
        private static MoveList moveList;

        static MoveListManager() {
            moveList = (MoveList) Resources.Load("Datas/MoveList");
        }

        public static MoveData GetMoveStats(int id) {
            if (id != -1) return moveList.Moves.FirstOrDefault(i => i.UniqueID == id);
            return new MoveData();
        }
    }
}
