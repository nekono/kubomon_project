﻿using System;
using UnityEngine;

namespace Systems.Move {
    /// <summary>
    /// 技情報
    /// </summary>
    [Serializable]
    public class MoveData {
        public string Name;
        public int UniqueID;
        public EType Type;
        public EMoveCategory Category;
        public int Priority;
        public int Power;
        public int PP;
        public int Accuracy;
        public float[] CorrectionValue;
        [Multiline(3)] public string Description;
        public MoveEffect[] MoveEffects;
    }
}
