namespace Systems.Move {
    /// <summary>
    /// 技の種類
    /// </summary>
    public enum EMoveCategory {
        None, Physical, Special,
        Status
    }
}
