﻿using System;

namespace Systems.Move {
    [Serializable]
    public class MoveSet {
        public Move[] MoveIDs = new Move[4];

        [Serializable]
        public struct Move {
            public int ID;
            public int AddPP;
            public int Phase;    //攻撃に補正がかかるフラグ管理

            public Move(int id = -1, int pp = 0) {
                ID = id;
                AddPP = pp;
                Phase = 0;
            }
        }
    }
}
