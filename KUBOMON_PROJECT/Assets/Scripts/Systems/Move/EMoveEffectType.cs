namespace Systems.Move {
    /// <summary>
    /// 技の追加効果の種類
    /// </summary>
    public enum EMoveEffectType {
        None, Buff, Debuff, StatusAilment, Change
    }
}
