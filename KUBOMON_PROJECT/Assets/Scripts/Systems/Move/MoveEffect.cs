using System;
using Systems.Kubomon;
using UnityEngine;

namespace Systems.Move {
    /// <summary>
    /// 技の追加効果
    /// </summary>
    [Serializable]
    public struct MoveEffect {
        public EMoveEffectType EffectType;
        [Range(0, 1), Tooltip("0が自分　1が相手")] public int Target; //対象
        public int Accuracy; //追加効果の確率
        public EStatsType StatsType; //
        [Range(0, 6)] public int Value; //追加効果の威力など(value段階あがる等)
        public EStatusAilment StatusAilment; //付与する状態異常の種類
    }
}
