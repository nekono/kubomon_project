using System.Collections.Generic;
using UnityEngine;

namespace Systems {
    [CreateAssetMenu(menuName = "Resources/Create TypeList",fileName = "TypeList")]
    public class TypeList : ScriptableObject {
        [SerializeField] private List<KubomonType> kubomonTypes = new List<KubomonType>();
        public List<KubomonType> KubomonTypes => kubomonTypes;
    }
}
