namespace Systems {
    /// <summary>
    /// 状態異常
    /// </summary>
    public enum EStatusAilment {
        None, Burn, Freeze,
        Paralysis, Poison, BadPoison,
        Sleep, Fainting
    }

    /// <summary>
    /// バトル中の状態異常
    /// 引っこめると直るやつ
    /// </summary>
    public enum EBattleAilment {
        None, Confusion, Curse,
        Encore, Flinch, Identify,
        Infatuation, LeechSeed, MindReader,
        LockOn, Nightmare, PartiallyTrapped,
        PerishSong, Taunt, Torment
    }
}
