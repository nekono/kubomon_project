using Systems.Move;

namespace Systems {
    /// <summary>
    /// 拡張メソッド用のクラス
    /// </summary>
    public static class Translate {
        public static readonly string[] TypeNames = {
            "None", "ノーマル", "ほのお", "みず", "でんき",
            "くさ", "こおり", "かくとう", "どく", "じめん",
            "ひこう", "エスパー", "むし", "いわ", "ゴースト",
            "ドラゴン", "あく", "はがね", "フェアリー"
        };

        public static readonly string[] AilmentNames = {
            "None", "やけど", "こおり", "まひ", "どく",
            "どく", "ねむり", "ひんし"
        };

        public static readonly string[] BattleAilmentName = {
            "None", "こんらん", "のろい", "アンコール", "ひるみ",
            "みやぶる", "メロメロ", "やどりぎのタネ", "こころのめ", "ロックオン",
            "あくむ", "しめつける", "ほろびのうた", "ちょうはつ", "いちゃもん"
        };

        public static readonly string[] MoveCatNames = {"None", "ぶつり", "とくしゅ", "へんか"};

        public static string GetJpName(this EType eType) {
            return TypeNames[(int) eType];
        }

        public static string GetJpName(this EStatusAilment eStatusAilment) {
            return AilmentNames[(int) eStatusAilment];
        }

        public static string GetJpName(this EBattleAilment eBattleAilment) {
            return BattleAilmentName[(int) eBattleAilment];
        }

        public static string GetJpName(this EMoveCategory eMoveCategory) {
            return MoveCatNames[(int) eMoveCategory];
        }
    }
}
