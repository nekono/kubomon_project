﻿using System.Linq;
using UnityEngine;

namespace Systems.Item {
    public static class ItemListManager {
        private static ItemList itemList;

        static ItemListManager() {
            itemList = (ItemList) Resources.Load("Datas/ItemList");
        }

        public static ItemData GetItemData(int id) {
            if (id != -1) return itemList.Items.FirstOrDefault(i => i.UniqueID == id);
            return new ItemData();
        }
    }
}
