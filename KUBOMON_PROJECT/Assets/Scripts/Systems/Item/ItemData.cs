﻿using System;

namespace Systems.Item {
    [Serializable]
    public struct ItemData {
        public string Name;
        public int UniqueID;
        public string Descriptions; //アイテムのゲーム内での説明文
        public int Value;           //いまいち仕様が固まってないが回復アイテムでは回復量といったアイテムごとの何かの要素の数値
    }
}
