﻿using System.Collections.Generic;
using UnityEngine;

namespace Systems.Item {
    [CreateAssetMenu(menuName =  "Resources/Create ItemList")]
    public class ItemList : ScriptableObject {
        [SerializeField] public List<ItemData> Items;
    }
}
