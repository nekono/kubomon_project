using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;

namespace Systems {
    public static class TypeListManager {
        private static readonly TypeList typeData;
        public static IReadOnlyList<KubomonType> KubomonTypes;

        static TypeListManager() {
            typeData = (TypeList) Resources.Load("Datas/TypeList");
            KubomonTypes = new ReadOnlyCollection<KubomonType>(typeData.KubomonTypes);
        }

        static KubomonType Search(EType typeName) {
            if (KubomonTypes.Any(i => i.Name == typeName)) {
                return KubomonTypes.First(i => i.Name == typeName);
            } else {
                Debug.AssertFormat(false, "{0} is not found", typeName);
                return new KubomonType();
            }
        }

        public static KubomonType GetType(EType typeName) {
            return Search(typeName);
        }

        public static string GetName(EType typeName) {
            return Search(typeName).Name.GetJpName();
        }
    }
}