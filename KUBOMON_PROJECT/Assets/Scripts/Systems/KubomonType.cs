using System;

namespace Systems {
    [Serializable]
    public struct KubomonType {
        public EType Name;
        public EType[] EffectiveType;
        public EType[] NotVeryEffectiveType;
        public EType[] IneffectiveType;
    }
}
