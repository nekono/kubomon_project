﻿using UnityEngine;

namespace Systems {
    //タイプの列挙型
    public enum EType {
        None, Normal, Fire,
        Water, Electric, Grass,
        Ice, Fighting, Poison,
        Ground, Flying, Psychic,
        Bug, Rock, Ghost,
        Dragon, Dark, Steel,Fairy
    }
}
