namespace Battle.UI {
    public interface IHPGauge {
        float MaxHP { get; }
        float CurrentHP { get; }
    }
}
