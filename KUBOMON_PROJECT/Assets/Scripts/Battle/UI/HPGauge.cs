using System;
using UnityEngine;
using UnityEngine.UI;

namespace Battle.UI {
    /// <summary>
    /// HPゲージの表示スクリプト
    /// IHPGaugeを実装したクラスのゲージを表示でき、増加・減少をなめらかに移動する
    /// また、色の変更も可能
    /// </summary>
    public class HPGauge : MonoBehaviour {
        [SerializeField] private Color maxColor = Color.green;
        [SerializeField] private Color halfColor = Color.yellow;
        [SerializeField] private Color pinchColor = Color.red;
        private Image image;
        private IHPGauge script;
        private bool isActive;

        private float startAmount;
        private float preAmount;
        

        private void Start() {
            image = GetComponent<Image>();
        }

        private void Update() {
            if (!isActive) return;
            var amount = script.CurrentHP / script.MaxHP;
            if (Math.Abs(preAmount - amount) > 0) startAmount = amount;
            if (Math.Abs(image.fillAmount - amount) > 0) {
                MoveGauge(amount);
            }
            UpdateColor();
            preAmount = amount;
        }

        private void UpdateColor() {
            image.color = image.fillAmount > 0.5f ? maxColor : image.fillAmount > 0.25f ? Color.yellow : Color.red;
        }

        private void MoveGauge(float amount) {
            float velocity = 0f;
            image.fillAmount = Mathf.SmoothDamp(startAmount, amount, ref velocity, 0.5f);
        }

        public void Init(IHPGauge _script) {
            script = _script;
            isActive = true;
            image.fillAmount = script.CurrentHP / script.MaxHP;
            UpdateColor();
        }

        public void Pause() {
            isActive = false;
        }

        public void Resume() {
            isActive = true;
        }

        public void Stop() {
            script = null;
            isActive = false;
        }
    }
}
