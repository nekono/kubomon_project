using System;
using UniRx;

namespace Battle.System {
    public interface IBattleCaller {
        EBattleType BattleType { get; set; }
        void BattleLaunch();
    }
}
