namespace Battle.System {
    public interface IBattleManager {
        EBattleType BattleType { get; }
        void BattleStart();
    }
}