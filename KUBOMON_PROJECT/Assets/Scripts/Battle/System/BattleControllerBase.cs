using UnityEngine;

namespace Battle.System {
    public abstract class BattleControllerBase : MonoBehaviour{
        protected EBattleType battleType;
        protected IBattleManager[] managers;
        
        public EBattleType BattleType => battleType;
        
        public abstract void BattleStart();
        public abstract void SetData(IBattleData data);
    }
}