using System.Linq;
using Systems;
using Animation;
using Player;
using UniRx;
using UnityEngine;

namespace Battle.System {
    public class BattleLauncher : SingletonMonoBehaviour<BattleLauncher> {
        private BattleControllerBase[] battleControllers;

        public void Launch(IBattleCaller caller) {
            if (caller is IWildBattleCaller) {
                var temp = caller as IWildBattleCaller;
                var controller = battleControllers.First(i => i is WildBattleController) as WildBattleController;
                if (controller != null) {
                    controller.SetData(new WildBattleData(new BattleKubomon(PlayerData.Instance.Party.First(i => i.HP > 0)),
                        new BattleKubomon(temp.SendData()),
                        controller.PlayerImage,
                        controller.EnemyImg));
                    AnimationController.Instance.RegisterAnimation(temp.SendAnimation());
                    temp.StartStream.First()
                        .Subscribe(_ => controller.BattleStart());
                } else {
                    Debug.LogError("WildBattleController is not found!");
                }
            }
        }

        private void Start() {
            battleControllers = GetComponentsInChildren<BattleControllerBase>();
        }
    }
}
