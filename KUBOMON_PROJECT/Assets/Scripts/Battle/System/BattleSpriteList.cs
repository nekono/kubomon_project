using UnityEngine;

namespace Battle.System {
    [CreateAssetMenu(menuName = "Resources/CreateBattleSpriteList")]
    public class BattleSpriteList : ScriptableObject {
        public Sprite[] TrainerFowardImg;
        public Sprite[] TrainerBackImgs;
        public Sprite[] GlassesImgs;
    }
}