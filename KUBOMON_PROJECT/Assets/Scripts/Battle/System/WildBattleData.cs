using Battle.Animation;
using Battle.Move;

namespace Battle.System {
    public class WildBattleData : IBattleData {
        public BattleKubomon PlayerKubomon { get; set; }
        public BattleKubomon EnemyKubomon { get; set; }
        public SingleTrainerImage PlayerImage { get; set; }
        public WildKubomonImage EnemyImage { get; set; }
        public TurnMoveBase PlayerMove { get; set; }
        public TurnMoveBase EnemyMove { get; set; }

        public WildBattleData(BattleKubomon playerKubomon, BattleKubomon enemyKubomon, SingleTrainerImage playerImage, WildKubomonImage enemyImage) {
            PlayerKubomon = playerKubomon;
            EnemyKubomon = enemyKubomon;
            PlayerImage = playerImage;
            EnemyImage = enemyImage;
            PlayerMove = null;
            EnemyMove = null;
        }
    }
}
