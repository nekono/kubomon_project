using Battle.Move;

namespace Battle.System {
    public interface IBattleData {
        BattleKubomon PlayerKubomon { get; set; }
        BattleKubomon EnemyKubomon { get; set; }
        TurnMoveBase PlayerMove { get; set; }
        TurnMoveBase EnemyMove { get; set; }
    }
}