using Battle.Animation;
using UnityEngine;

namespace Battle.System {
    public class WildBattleController : BattleControllerBase {
        private WildBattleData battleData;

        [SerializeField] private SingleTrainerImage playerImage;
        [SerializeField] private WildKubomonImage enemyImg;
            
        public WildBattleData BattleData => battleData;
        public SingleTrainerImage PlayerImage => playerImage;
        public WildKubomonImage EnemyImg => enemyImg;

        private void Start() {
            battleType = EBattleType.Wild;
            managers = GetComponentsInChildren<IBattleManager>();
        }

        public override void BattleStart() {
            foreach (var manager in managers) {
                manager.BattleStart();
            }
        }

        public override void SetData(IBattleData data) {
            if (data is WildBattleData) {
                battleData = data as WildBattleData;
            } else {
                Debug.LogError($"{data} is not WildBattleData!");
            }
        }
    }
}
