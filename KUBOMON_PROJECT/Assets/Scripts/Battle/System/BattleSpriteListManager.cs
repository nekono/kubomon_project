using UnityEngine;

namespace Battle.System {
    public static class BattleSpriteListManager {
        private static BattleSpriteList spriteList;

        static BattleSpriteListManager() {
            spriteList = (BattleSpriteList) Resources.Load("Datas/BattleSpriteList");
        }

        public static Sprite GetTrainerBackSprite(int id) {
            return spriteList.TrainerBackImgs[id];
        }

        public static Sprite GetTrainerFowardSprite(int id) {
            return spriteList.TrainerFowardImg[id];
        }

        public static Sprite GetGlasseSprite(int id) {
            return spriteList.GlassesImgs[id];
        }
    }
}