using System.Collections.Generic;
using Systems;
using Systems.Kubomon;
using Systems.Move;

namespace Battle.System {
    /// <summary>
    /// 戦闘中のクボモン
    /// 手持ちからコピーしたりして使う
    /// </summary>
    public class BattleKubomon : TrainersKubomonStats {
        public int AttackRank;
        public int DefenseRank;
        public int SpAttackRank;
        public int SpDefenseRank;
        public int SpeedRank;
        public int AccuracyRank;
        public int EvasionRank;
        public List<EBattleAilment> BattleAilments = new List<EBattleAilment>();
        
        public BattleKubomon(KubomonStats kubomonStats) : base(kubomonStats) {
            AttackRank = 0;
            DefenseRank = 0;
            SpAttackRank = 0;
            SpDefenseRank = 0;
            SpeedRank = 0;
            AccuracyRank = 0;
            EvasionRank = 0;
        }

        public BattleKubomon(TrainersKubomonStats trainersKubomonStats) : base(trainersKubomonStats) {
            AttackRank = 0;
            DefenseRank = 0;
            SpAttackRank = 0;
            SpDefenseRank = 0;
            SpeedRank = 0;
            AccuracyRank = 0;
            EvasionRank = 0;
        }
    } 
}
