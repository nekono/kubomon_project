using Systems.Kubomon;
using Animation;
using UniRx;

namespace Battle.System {
    public interface IWildBattleCaller : IBattleCaller {
        KubomonStats[] Kubomons { get; set; }
        AnimationBase[] EncountAnim { get; set; }
        Subject<Unit> StartStream { get; }
        KubomonStats SendData();
        AnimationBase SendAnimation();
    }
}
