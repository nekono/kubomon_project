using UnityEngine.UI;

namespace Battle.Animation {
    public interface IBattleImage {
        Image KubomonImg { get; set; }
    }
}