using Systems.Kubomon;

namespace Battle.Animation {
    /*
    public class WildBattleAnimator : BattleAnimatorBase {
        private WildBattleData battleData;
        
        

        public override void BattleStart() {
            base.BattleStart();
            var playerSpriteNum = 0;
            battleData.PlayerImage.TrainerImg.sprite = BattleSpriteListManager.GetTrainerBackSprite(playerSpriteNum);
        }
    }
    
    public abstract class OldBattleAnimatorBase : AnimationBase {
        [SerializeField] protected AnimationSetting[] Animations;

        private PlayableAsset[] specialAnim = new PlayableAsset[10];
        private int currentAnimNum;
        protected AnimationSetting CurrentAnimation;
        protected bool Tapflg;
        protected BattleText BattleText { get; private set; }
        protected BattleImage BattleImage { get; private set; }

        private BattleControllerBase battleManager;

        public void Init(BattleControllerBase manager) {
            battleManager = manager;
            BattleText.Init();
        }

        public virtual void BattleStart(PlayableAsset[] anims = null, IBattleData battleData = null) {
            currentAnimNum = 0;
            BattleText.BattleStart();
            if (anims != null) Array.Copy(anims, specialAnim, anims.Length);
            SetupAnimation(ref Animations[0], specialAnim[0]);
            SetupAnimation(ref Animations[1], specialAnim[1]);
        }

        protected void SetupAnimation(ref AnimationSetting animationSetting, PlayableAsset anim = null) {
            animationSetting.Animation = anim != null ? anim : animationSetting.DefaultAnimation;
            AnimationController.Instance.RegisterAnimation(this, false, animationSetting.Animation);
        }

        public override void PlayAnim(PlayableAsset asset = null) {
            CurrentAnimation = Animations.First(i => i.Animation == asset);
            Tapflg = !CurrentAnimation.NeedTap;
            BattleText.UpdateText(currentAnimNum);
            currentAnimNum++;
            base.PlayAnim(asset);
        }

        protected override void EndCheck() {
            if (Input.anyKeyDown) Tapflg = true;
            if (director.state != PlayState.Playing && Tapflg && BattleText.IsEnd()) {
                BattleText.GoNext();

                IsPlaying = false;
            }
        }

        [Serializable]
        protected struct AnimationSetting {
            public PlayableAsset DefaultAnimation;
            public bool NeedTap;
            [NonSerialized] public PlayableAsset Animation;
        }
    }*/

    public static class BattleTextTemplate {
        public static readonly string WildBattleStartText = "あ！ やせいの\n{0}が とびだしてきた！";
        public static readonly string SingleBattleStartText = "{0}の {1}が\nしょうぶを しかけてきた！";
        public static readonly string DoubleBattleStartText = "{0}の {1}と{2}が\nしょうぶを しかけてきた！";
        public static readonly string SingleEnemySetup = "{0}の {1}は\n{2}を くりだした！";
        public static readonly string DoubleEnemySetupText = "{0}の{1}と{2}は\n{3}と{4}を くりだした！";
        public static readonly string SinglePlayerSetupText = "ゆけっ！{0}!";
        public static readonly string DoublePlayerSetupText = "ゆけっ！{0}！{1}！";
        public static readonly string MoveSelectText = "{0}は どうする？";
        public static readonly string[] Moves = {"たたかう", "バッグ", "クボモン", "にげる"};
        public static readonly string[] MoveText = {"{0}{1}の\n {2}　こうげき！", "{0}{1}は\n {2}を つかった！", "{0}{1}の\n {2}！"};
        public static readonly string CriticalHitText = "きゅうしょに あたった！";
        public static readonly string EffectiveText = "こうかは ばつぐんだ！";
        public static readonly string NotVeryEffectiveText = "こうかは いまひとつのようだ";
        public static readonly string UnEffectiveText = "{0}には\nこうかがない みたいだ...";
        public static readonly string DeBuffText = "{0}{1}の\n{2}が さがった！";
        public static readonly string BuffText = "{0}{1}の\n{2}が あがった！";
        public static readonly string DownText = "{0}{1}は たおれた！";
        public static readonly string ExpText = "{0}は\n{1} けいけんちを もらった！";
        public static readonly string LevelUpText = "{0}は\nレベル{1} に あがった！";
        public static readonly string ChangeText0 = "{0}の {1}は\nをくりだそうと している。";
        public static readonly string ChangeText1 = "{0}も クボモンを いれかえますか？";
        public static readonly string BackText = "もどれ！{0}！";
        public static readonly string SendText = "いってこい！ {0}！";
        public static readonly string ItemText = "{0}は {1}に\n{2}を つかった！";
        public static readonly string RunText = "うまく にげきれた！";
        public static readonly string RunFailText = "にげられない！";
        public static readonly string BattleRunText = "ダメだ！ しょうぶの　さいちゅうに\nあいてに せなかは みせられない！";
        public static readonly string SingleBattleWinText = "{0}の {1}\nとの しょうぶに かった！";
        public static readonly string DoubleBattleWinText = "{0}の {1}と{2}\nとの しょうぶに かった！";
        public static readonly string BattleEndText = "{0}";
        public static readonly string ResultText = "{0}は しょうきんとして\n{1}円 てにいれた！";
        public static readonly string WildKubomon = "やせいの ";
        public static readonly string EnemyKubomon = "あいての ";
        public static readonly string PlayerKubomon = "";
    }
}
