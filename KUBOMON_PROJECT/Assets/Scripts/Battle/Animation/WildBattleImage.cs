using System;
using UnityEngine.UI;

namespace Battle.Animation {
    [Serializable]
    public struct WildKubomonImage : IBattleImage {
        public Image KubomonImg { get; set; }
        public WildKubomonImage(Image kubomonImg) {
            KubomonImg = kubomonImg;
        }
    }
}