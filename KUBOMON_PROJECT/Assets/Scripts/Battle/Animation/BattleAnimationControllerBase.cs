using System;
using System.Collections.Generic;
using System.Linq;
using Animation;
using Battle.System;
using UniRx;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace Battle.Animation {
    public abstract class BattleAnimationControllerBase : MonoBehaviour, IBattleManager {
        protected BattleText battleText { get; private set; }
        protected BattleAnimator battleAnimator { get; private set; }
        protected string[] texts;
        protected PlayableAsset[] animations;
        protected int animationIndex;
        protected EBattleType battleType;

        protected bool isEnd;

        private bool isActive;
        private Canvas myCanvas;
        private Camera main;
        private List<Tuple<GraphicRaycaster, bool>> raycasters;

        public PlayableAsset[] Animations => animations;
        public EBattleType BattleType => battleType;


        protected virtual void Start() {
            battleAnimator = GetComponent<BattleAnimator>();
            battleText = GetComponent<BattleText>();
            myCanvas = GetComponent<Canvas>();
            main = Camera.main;

            Observable.Zip(battleAnimator.EndStream, battleText.EndStream)
                .Subscribe(_ => isEnd = true);
        }

        protected virtual void Update() {
            if (!isActive) return;
        }

        protected void PlayStart() {
            battleText.PlayStart(texts[animationIndex]);
            isEnd = false;
            if (animations[animationIndex] != null) {
                AnimationController.Instance.RegisterAnimation(battleAnimator, false, animations[animationIndex]);
            }
        }

        public virtual void BattleStart() {
            isActive = true;
            animationIndex = 0;
            PlayStart();
            SetupCamera();
        }

        public void BattleEnd() {
            raycasters.ForEach(i => i.Item1.enabled = i.Item2);
            myCanvas.worldCamera.gameObject.SetActive(false);
            if (main != null) main.gameObject.SetActive(true);
        }

        private void SetupCamera() {
            main = Camera.main;
            if (main != null) main.gameObject.SetActive(false);
            myCanvas.worldCamera.gameObject.SetActive(true);
            raycasters = FindObjectsOfType<GraphicRaycaster>().Select(i => new Tuple<GraphicRaycaster, bool>(i, i.enabled)).ToList();
            raycasters.ForEach(i => i.Item1.enabled = false);
            GetComponent<GraphicRaycaster>().enabled = true;
        }
    }

    public class WildBattleAnimationController : BattleAnimationControllerBase {
        protected override void Update() {
            base.Update();
            if (isEnd) {
                if (Input.anyKeyDown) {
                    switch (animationIndex) {
                        case 0:
                            animationIndex++;
                            PlayStart();
                            break;
                    }
                }
            }
        }
    }
}
