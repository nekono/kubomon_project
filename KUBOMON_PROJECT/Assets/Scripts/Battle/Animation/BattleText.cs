using Animation;
using UnityEngine;
using UnityEngine.UI;

namespace Battle.Animation {
    public class BattleText : TextAnimationBase {
        [SerializeField] private GameObject buttonsRoot;
        private Text[] moveButtons;

        private void Start() {
            moveButtons = buttonsRoot.GetComponentsInChildren<Text>();
        }

        public void ResetButton() {
            moveButtons[0].text = BattleTextTemplate.Moves[0];
            moveButtons[1].text = BattleTextTemplate.Moves[1];
            moveButtons[2].text = BattleTextTemplate.Moves[2];
            moveButtons[3].text = BattleTextTemplate.Moves[3];
        }
    }
}