using System;
using UnityEngine.UI;

namespace Battle.Animation {
    [Serializable]
    public struct SingleTrainerImage : IBattleImage {
        public Image KubomonImg { get; set; }
        public Image TrainerImg;
        public Image BallImg;

        public SingleTrainerImage(Image kubomonImg, Image trainerImg, Image ballImg) {
            KubomonImg = kubomonImg;
            TrainerImg = trainerImg;
            BallImg = ballImg;
        }
    }
}