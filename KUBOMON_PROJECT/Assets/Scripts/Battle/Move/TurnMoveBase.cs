namespace Battle.Move {
    public abstract class TurnMoveBase {
        public EMoveType MoveType { get; protected set; }
    }
}