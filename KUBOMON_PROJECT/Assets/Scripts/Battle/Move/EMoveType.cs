namespace Battle.Move {
    public enum EMoveType {
        Attack,
        Item,
        Change,
        Run
    }
}