namespace Battle.Move {
    public class Attack : TurnMoveBase {
        public readonly int MoveId;
        public readonly int StateNumber;

        public Attack(int moveId, int stateNumber = 0) {
            MoveType = EMoveType.Attack;
            MoveId = moveId;
            StateNumber = stateNumber;
        }
    }
}