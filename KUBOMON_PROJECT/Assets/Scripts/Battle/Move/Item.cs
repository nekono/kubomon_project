using Systems.Kubomon;

namespace Battle.Move {
    public class Item : TurnMoveBase {
        public readonly int ItemId;
        public readonly KubomonStats Target;

        public Item(int itemId, KubomonStats target) {
            MoveType = EMoveType.Item;
            ItemId = itemId;
            Target = target;
        }
    }
}