using Systems.Kubomon;

namespace Battle.Move {
    public class Change : TurnMoveBase {
        public readonly KubomonStats Target;

        public Change(KubomonStats target) {
            Target = target;
        }
    }
}