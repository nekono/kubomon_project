﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using Systems;
using UnityEngine.Tilemaps;
using Map;

namespace Editor
{
    public class ClearTileMap
    {
        static GameObject Deep;
        static GameObject Under;
        static GameObject Front;

        static DrawMap drawmap_d;
        static DrawMap drawmap_du;
        static DrawMap drawmap_f;



        [MenuItem("Editor/###ClearTileMap###")]
        private static void Create()
        {
            EditorMapClear();
        }

        private static void EditorMapClear()
        {
            //描画用のレイヤーオブジェクトを取得
            Deep = GameObject.Find("Deep");
            Under = GameObject.Find("Under");
            Front = GameObject.Find("Front");

            drawmap_d = Deep.GetComponent<DrawMap>();
            drawmap_du = Under.GetComponent<DrawMap>();
            drawmap_f = Front.GetComponent<DrawMap>();

            drawmap_d.ClearTile();
            drawmap_du.ClearTile();
            drawmap_f.ClearTile();
        }
    }
}
