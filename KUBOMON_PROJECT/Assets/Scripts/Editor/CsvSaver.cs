using System.Collections.Generic;
using System.IO;
using System.Text;
using Systems;
using Systems.Kubomon;
using UnityEditor;
using UnityEngine;

namespace Editor {
    public class CsvSaver {
        [MenuItem("Editor/SaveToCsv")]
        public static void SaveToCsv() {
            Save(KuboListManager.GetAllKuboData());
        }

        public static void Save(List<KuboData> kuboDatas) {
            StreamWriter sw = new StreamWriter(Application.dataPath + "/Objects/Texts/KuboDatas.csv", false, Encoding.UTF8);

            string[] head = {"クボモン名", "フォルム名", "固有ID", "図鑑番号", "タイプ1(数字)", "タイプ2(数字)", "H", "A", "B", "C", "D", "S", "特性名1", "特性名2", "特性名3"};
            sw.WriteLine(string.Join(",", head));
            foreach (var d in kuboDatas) {
                string[] str = {d.Name, d.FormName, d.UniqueID.ToString(), d.DexID.ToString(), ((int) d.Type1).ToString(), ((int) d.Type2).ToString(), d.BaseStats.H.ToString(), d.BaseStats.A.ToString(), d.BaseStats.B.ToString(), d.BaseStats.C.ToString(), d.BaseStats.D.ToString(), d.BaseStats.S.ToString(), d.AbilityName1, d.AbilityName2, d.AbilityName3};
                sw.WriteLine(string.Join(",", str));
            }

            sw.Close();
        }
    }
}
