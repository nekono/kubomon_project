﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using Systems;
using UnityEngine.Tilemaps;
using Map;

namespace Editor
{
    public class CsvMapDrawer
    {
        //タイルの数
        public const int number_of_tiles = 2048;

        //ワールド全体のmapIDを取得
        private static string[,] mapID_datas = new string[30, 30];

        //private Sprite[] sprite;
        public static Tile[] tile = new Tile[number_of_tiles];

        static GameObject Deep;
        static GameObject Under;
        static GameObject Front;

        static DrawMap drawmap_d;
        static DrawMap drawmap_du;
        static DrawMap drawmap_f;



        [MenuItem("Editor/###CsvMapDrawer###")]
        private static void Create()
        {
            EditorMapDataLoader();
        }



        private static void EditorMapDataLoader()
        {
            //ワールド全体を構成するIDデータを取得
            mapID_datas = EditorLoadMapData("Map_ID", 30);

            //タイルを順番にロードする、LoadAllだとグチャる
            for (int i = 0; i < number_of_tiles; i++)
            {
                string f = "Tile/image_" + i.ToString();
                tile[i] = Resources.Load<Tile>(f);
            }

            //描画用のレイヤーオブジェクトを取得
            Deep = GameObject.Find("Deep");
            Under = GameObject.Find("Under");
            Front = GameObject.Find("Front");

            drawmap_d = Deep.GetComponent<DrawMap>();
            drawmap_du = Under.GetComponent<DrawMap>();
            drawmap_f = Front.GetComponent<DrawMap>();

            drawmap_d.DrawingMapAll();
            drawmap_du.DrawingMapAll();
            drawmap_f.DrawingMapAll();
        }


        //Map_Data内のファイル名を入れるとcsvをList<string[]>で返す
        private static string[,] EditorLoadMapData(string fileName, int size)
        {
            //一時的なデータ入れ
            string[,] datas = new string[size, size];

            // Resourcesのcsvフォルダ内のcsvファイルをTextAssetとして取得
            var csvFile = Resources.Load("Map_Data/" + fileName) as TextAsset;

            if (csvFile == null)
            {
                csvFile = Resources.Load("Map_Data/Map_0_0f") as TextAsset;
            }

            // csvファイルの内容をStringReaderに変換
            var reader = new StringReader(csvFile.text);

            int j = size - 1;
            // csvファイルの内容を一行ずつ末尾まで取得しリストを作成
            while (reader.Peek() > -1)
            {
                // 一行読み込む
                var lineData = reader.ReadLine();
                // カンマ(,)区切りのデータを文字列の配列に変換
                var address = lineData.Split(',');

                //配列に追加
                for (int i = 0; i < size; i++)
                {
                    datas[i, j] = address[i];
                }

                j--;
                // 末尾まで繰り返し...
            }

            return datas;
        }
    }
}
