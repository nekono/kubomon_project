﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Editor
{
    public class EditorTest
    {
        [MenuItem("Test/Create")]
        public static void Create()
        {
            //新規オブジェクトの生成
            GameObject newGameObject = new GameObject("New TestObject");
            Undo.RegisterCreatedObjectUndo(newGameObject, "Create New Object"); //アンドゥの履歴に登録

        }

        [MenuItem("Test/Create2")]
        public static void Create2()
        {
            //新規オブジェクトの生成
            GameObject newGameObject = new GameObject("New TestObject2");
            Undo.RegisterCreatedObjectUndo(newGameObject, "Create New Object"); //アンドゥの履歴に登録
        }
    }
}
