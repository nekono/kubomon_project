﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Systems;
using Systems.Kubomon;

namespace Editor {
    /// <summary>
    /// KuboList(旧Kubodex)を編集するエディター拡張
    /// </summary>
    public class KubodexEditor : EditorWindow {
        private EMenus menu;
        private static GUIStyle menuStyle;
        private static GUIStyle fieldStyle;
        private static GUIStyle buttonStyle;
        private static List<KuboData> datas;
        private static Vector2 scrollPos;
        private static int selectedNum;
        private static int preSelectedNum;
        private static KuboData localData;
        private static KuboData preData;


        /// <summary>
        /// 開いた時の動き
        /// </summary>
        [MenuItem("Editor/KuboListEditor")]
        static void Open() {
            Setup();
            var w = GetWindow<KubodexEditor>(true, "KuboListEditor");
            w.maxSize = w.minSize = new Vector2(960, 540);
        }

        /// <summary>
        /// 設定項目
        /// 目次みたいなもの
        /// </summary>
        enum EMenus {
            KubomonSetting, StatsSetting, ImgSetting
        }

        /// <summary>
        /// 初期化
        /// csvファイルを反映させる
        /// データを変数に格納する
        /// Styleの設定
        /// </summary>
        private static void Setup() {
            CsvToKubodex();
            datas = new List<KuboData>(KuboListManager.GetAllKuboData());
            if (datas.Count > 0) {
                localData = datas[0];
                preSelectedNum = 0;
                selectedNum = 0;
                UpdateKuboData();
            }

            menuStyle = new GUIStyle("GameViewBackground") {onNormal = {background = GetTexture(new Color32(66, 96, 147, 255))}, active = {background = GetTexture(new Color32(85, 85, 85, 255))}, fixedHeight = 30, normal = {textColor = Color.white}};
            fieldStyle = new GUIStyle(menuStyle) {focused = {background = GetTexture(Color.gray)}, active = {background = GetTexture(Color.gray)}, fixedWidth = 180, fixedHeight = 30};
            buttonStyle = new GUIStyle("PreButton") {fixedHeight = 30};
        }

        /// <summary>
        /// ウィンドウ内での動き
        /// </summary>
        private void OnGUI() {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), GetTexture(new Color32(85, 85, 85, 255))); //背景
            GUILayout.BeginHorizontal();
            {
                DrawMenuBox();
                GUILayout.BeginVertical();
                {
                    GUILayout.Space(30);
                    DrawMenus();
                    GUILayout.EndVertical();
                }
            }
            GUILayout.EndHorizontal();
            if (GUI.Button(new Rect(0, 510, 120, 30), "Save and Close", menuStyle)) {
                Save();
                Close();
            }
        }

        /// <summary>
        /// 目次の表示
        /// </summary>
        void DrawMenuBox() {
            GUILayout.BeginVertical(GUILayout.Width(120));
            {
                GUILayout.Space(30);
                EditorGUIUtility.labelWidth = 120f;
                GUI.Label(new Rect(0, 0, 120, Screen.height), "", "GameViewBackground");
                menu = (EMenus) GUILayout.SelectionGrid((int) menu, Enum.GetNames(typeof(EMenus)), 1, menuStyle);
            }
            GUILayout.EndVertical();
        }

        /// <summary>
        /// メニューごとの動き
        /// </summary>
        void DrawMenus() {
            switch (menu) {
                case EMenus.KubomonSetting: {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(20);
                        GUI.Label(new Rect(140, 30, 300, 480), "", "GameViewBackground"); //左側の背景
                        DrawKuboList();
                        GUILayout.Space(20);
                        GUILayout.BeginVertical(GUILayout.Width(120));
                        {
                            GUILayout.Space(180);
                            //データの追加
                            if (GUILayout.Button("Add", buttonStyle, GUILayout.Width(100))) {
                                datas.Add(new KuboData("New Kubomon"));
                                if (datas.Count == 1) {
                                    localData = datas[0];
                                    preSelectedNum = 0;
                                    selectedNum = 0;
                                    UpdateKuboData();
                                }
                            }

                            //データの削除
                            if (GUILayout.Button("Delete", buttonStyle, GUILayout.Width(100))) {
                                if (datas.Count == 0) return;
                                datas.Remove(datas.First(x => x.UniqueID == datas[selectedNum].UniqueID));
                                if (datas.Count > 0) {
                                    localData = datas[0];
                                    preSelectedNum = 0;
                                    selectedNum = selectedNum >= datas.Count ? datas.Count - 1 : selectedNum;
                                    UpdateKuboData();
                                }
                            }
                        }
                        GUILayout.EndVertical();
                        DrawKuboData();
                    }
                    GUILayout.EndHorizontal();
                    break;
                }
                case EMenus.StatsSetting: {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(20);
                        GUI.Label(new Rect(140, 30, 300, 480), "", "GameViewBackground"); //左側の背景
                        DrawKuboList();
                        GUILayout.Space(20);
                        GUILayout.Space(120);
                        DrawKuboBaseStats();
                    }
                    GUILayout.EndHorizontal();
                    break;
                }
                case EMenus.ImgSetting: {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Space(20);
                        GUI.Label(new Rect(140, 30, 300, 480), "", "GameViewBackground"); //左側の背景
                        DrawKuboList();
                        GUILayout.Space(20);
                        GUILayout.Space(120);
                        DrawKuboImgs();
                    }
                    GUILayout.EndHorizontal();
                    break;
                }
            }
        }

        /// <summary>
        /// クボモン一覧表示
        /// </summary>
        void DrawKuboList() {
            scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Height(480), GUILayout.Width(300));
            preSelectedNum = selectedNum;
            selectedNum = GUILayout.SelectionGrid(selectedNum, datas.Select(x => x.Name).ToArray(), 1, menuStyle, GUILayout.Width(300));
            if (preSelectedNum != selectedNum) {
                UpdateKuboData();
            }

            GUILayout.EndScrollView();
        }

        /// <summary>
        /// データの更新
        /// </summary>
        static void UpdateKuboData() {
            datas[preSelectedNum] = localData; //編集データの保存
            preData = datas[selectedNum]; //編集前データの更新
            localData = preData; //編集データの初期化
        }

        /// <summary>
        /// 選択されたクボデータの表示＆編集
        /// </summary>
        void DrawKuboData() {
            GUI.Label(new Rect(580, 30, 360, 480), "", "GameViewBackground");
            GUILayout.BeginVertical(GUILayout.Width(180));
            {
                var t = typeof(KuboData);
                var fields = t.GetFields();
                foreach (var m in fields) {
                    GUILayout.Label(m.Name, new GUIStyle() {font = menuStyle.font, normal = {textColor = Color.white}}, GUILayout.Height(30));
                }
            }
            GUILayout.EndVertical();
            if (datas.Count == 0) return;
            GUILayout.BeginVertical();
            {
                localData.Name = GUILayout.TextArea(localData.Name, fieldStyle, GUILayout.Height(30));
                localData.FormName = GUILayout.TextArea(localData.FormName, fieldStyle, GUILayout.Height(30));
                localData.UniqueID = EditorGUILayout.IntField(localData.UniqueID, fieldStyle, GUILayout.Height(30));
                localData.DexID = EditorGUILayout.IntField(localData.DexID, fieldStyle, GUILayout.Height(30));
                localData.Type1 = (EType) Enum.ToObject(typeof(EType), EditorGUILayout.Popup((int) localData.Type1, Translate.TypeNames, fieldStyle, GUILayout.Height(30)));
                localData.Type2 = (EType) Enum.ToObject(typeof(EType), EditorGUILayout.Popup((int) localData.Type2, Translate.TypeNames, fieldStyle, GUILayout.Height(30)));
                if (GUILayout.Button("Edit BaseStats", buttonStyle, GUILayout.Width(180))) {
                    UpdateKuboData();
                    menu = EMenus.StatsSetting;
                }

                localData.AbilityName1 = GUILayout.TextArea(localData.AbilityName1, fieldStyle, GUILayout.Height(30));
                localData.AbilityName2 = GUILayout.TextArea(localData.AbilityName2, fieldStyle, GUILayout.Height(30));
                localData.AbilityName3 = GUILayout.TextArea(localData.AbilityName3, fieldStyle, GUILayout.Height(30));
                if (GUILayout.Button("Edit Imgs", buttonStyle, GUILayout.Width(180))) {
                    UpdateKuboData();
                    menu = EMenus.ImgSetting;
                }

                //キャンセル処理
                if (GUILayout.Button("Reset", buttonStyle, GUILayout.Width(180))) {
                    localData = preData;
                }
            }
            GUILayout.EndVertical();
        }

        /// <summary>
        /// 選択されたクボモンの種族値表示＆編集
        /// </summary>
        void DrawKuboBaseStats() {
            GUI.Label(new Rect(580, 30, 360, 480), "", "GameViewBackground");
            GUILayout.BeginVertical(GUILayout.Width(30));
            {
                var t = typeof(BaseStats);
                var fields = t.GetFields();
                foreach (var m in fields) {
                    GUILayout.Label(m.Name, new GUIStyle {font = menuStyle.font, normal = {textColor = Color.white}}, GUILayout.Height(30));
                }
            }
            GUILayout.EndVertical();
            if (datas.Count == 0) return;
            GUILayout.BeginVertical();
            {
                localData.BaseStats.H = EditorGUILayout.IntSlider(localData.BaseStats.H, 0, 255, GUILayout.Height(30), GUILayout.Width(300));
                localData.BaseStats.A = EditorGUILayout.IntSlider(localData.BaseStats.A, 0, 255, GUILayout.Height(30), GUILayout.Width(300));
                localData.BaseStats.B = EditorGUILayout.IntSlider(localData.BaseStats.B, 0, 255, GUILayout.Height(30), GUILayout.Width(300));
                localData.BaseStats.C = EditorGUILayout.IntSlider(localData.BaseStats.C, 0, 255, GUILayout.Height(30), GUILayout.Width(300));
                localData.BaseStats.D = EditorGUILayout.IntSlider(localData.BaseStats.D, 0, 255, GUILayout.Height(30), GUILayout.Width(300));
                localData.BaseStats.S = EditorGUILayout.IntSlider(localData.BaseStats.S, 0, 255, GUILayout.Height(30), GUILayout.Width(300));

                //キャンセル処理
                if (GUILayout.Button("Reset", buttonStyle, GUILayout.Width(100))) {
                    localData.BaseStats = preData.BaseStats;
                }
            }
            GUILayout.EndVertical();
        }

        /// <summary>
        /// 選択されたクボモンの画像表示＆編集
        /// </summary>
        void DrawKuboImgs() {
            GUI.Label(new Rect(580, 30, 360, 480), "", "GameViewBackground");
            GUILayout.BeginVertical(GUILayout.Width(100));
            {
                var t = typeof(KuboImg);
                var fields = t.GetFields();
                foreach (var m in fields) {
                    GUILayout.Label(m.Name, new GUIStyle {font = menuStyle.font, normal = {textColor = Color.white}}, GUILayout.Height(100));
                }
            }
            GUILayout.EndVertical();
            if (datas.Count == 0) return;
            GUILayout.BeginVertical();
            {
                localData.KuboImg.Foward = (Sprite) EditorGUILayout.ObjectField(localData.KuboImg.Foward, typeof(Sprite), false, GUILayout.Height(100), GUILayout.Width(100));
                localData.KuboImg.Back = (Sprite) EditorGUILayout.ObjectField(localData.KuboImg.Back, typeof(Sprite), false, GUILayout.Height(100), GUILayout.Width(100));
                localData.KuboImg.SDImage = (Sprite) EditorGUILayout.ObjectField(localData.KuboImg.SDImage, typeof(Sprite), false, GUILayout.Height(100), GUILayout.Width(100));
                //キャンセル処理
                if (GUILayout.Button("Reset", buttonStyle, GUILayout.Width(100))) {
                    localData.KuboImg = preData.KuboImg;
                }
            }
            GUILayout.EndVertical();
        }


        /// <summary>
        /// csvからデータの更新
        /// </summary>
        static void CsvToKubodex() {
            KuboListManager.UpdateKuboData(CsvLoader.Load());
        }

        /// <summary>
        /// データの反映とcsvセーブ
        /// </summary>
        static void Save() {
            if (datas.Count > 0) UpdateKuboData();
            CsvSaver.Save(datas); //csvにセーブする
            KuboListManager.UpdateKuboData(datas, true); //データを反映する
        }

        /// <summary>
        /// 引数の色のテクスチャ作るやつ
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static Texture2D GetTexture(Color color) {
            var texture = new Texture2D(1, 1);
            texture.SetPixel(0, 0, color);
            texture.Apply();
            return texture;
        }
    }
}
