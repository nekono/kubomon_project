﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Editor {
    public class MiniWindowTest : EditorWindow {

        static MiniWindowTest window;

        [MenuItem("Test/MiniWindow")]
        public static void Open()
        {
            if (window == null){ window = CreateInstance<MiniWindowTest>(); }
            window.ShowUtility();


            //GetWindow<MiniWindowTest>();  この一行で完結するが常に全面表示する"ShowUtility"が使用できないので却下
        }
    }
}
