using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Systems;
using UnityEditor;
using UnityEngine;

namespace Editor {
    /// <summary>
    /// データを復元するクラス
    /// </summary>
    public class DataRestore {
        static readonly string SourcePath = Application.dataPath + "/Resources/KuboDatas/KuboList.asset";
        static readonly string CopyPath = Application.dataPath + "/Objects/Texts/KuboList.asset_BuckUp.txt";

        [MenuItem("Editor/Data/Restore")]
        public static void Restore() {
            AssetDatabase.SaveAssets ();
            try {
                FileUtil.DeleteFileOrDirectory(SourcePath);
                FileUtil.DeleteFileOrDirectory(SourcePath + ".meta");

                AssetDatabase.Refresh();
            }
            finally {
                FileUtil.CopyFileOrDirectory(CopyPath, SourcePath);
            }

            AssetDatabase.Refresh();
        }

        [MenuItem("Editor/Data/BackUp")]
        public static void Save() {
            AssetDatabase.SaveAssets ();
            try {
                FileUtil.DeleteFileOrDirectory(CopyPath);
                FileUtil.DeleteFileOrDirectory(CopyPath + ".meta");

                AssetDatabase.Refresh();
            }
            finally {
                FileUtil.CopyFileOrDirectory(SourcePath, CopyPath);
            }

            AssetDatabase.Refresh();
        }
    }
}
