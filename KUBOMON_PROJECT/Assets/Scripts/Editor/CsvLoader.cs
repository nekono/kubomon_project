using System;
using System.Collections.Generic;
using System.IO;
using Systems;
using Systems.Kubomon;
using UnityEngine;

namespace Editor {
    public class CsvLoader {
        public static List<KuboData> Load() {
            List<KuboData> kuboDatas = new List<KuboData>();
            StreamReader sr = new StreamReader(Application.dataPath + "/Objects/Texts/KuboDatas.csv");
            if (!sr.EndOfStream) sr.ReadLine();
            while (!sr.EndOfStream) {
                string line = sr.ReadLine();
                string[] csvDatas = line.Split(',');
                if (csvDatas.Length < 1) continue;
                int result = 0;
                var name = csvDatas[0];
                var formName = csvDatas[1];
                var id = int.TryParse(csvDatas[2], out result) ? result : -1;
                var dexId = int.TryParse(csvDatas[3], out result) ? result : -1;
                var type1 = (EType) Enum.ToObject(typeof(EType), int.TryParse(csvDatas[4], out result) ? result : 0);
                var type2 = (EType) Enum.ToObject(typeof(EType), int.TryParse(csvDatas[5], out result) ? result : 0);
                BaseStats baseStats;
                {
                    var h = int.TryParse(csvDatas[6], out result) ? result : 0;
                    var a = int.TryParse(csvDatas[7], out result) ? result : 0;
                    var b = int.TryParse(csvDatas[8], out result) ? result : 0;
                    var c = int.TryParse(csvDatas[9], out result) ? result : 0;
                    var d = int.TryParse(csvDatas[10], out result) ? result : 0;
                    var s = int.TryParse(csvDatas[11], out result) ? result : 0;
                    baseStats = new BaseStats(h, a, b, c, d, s);
                }
                var abilityName1 = csvDatas[12];
                var abilityName2 = csvDatas[13];
                var abilityName3 = csvDatas[14];
                var data = new KuboData(name, formName, id, dexId, type1, type2, baseStats, abilityName1, abilityName2, abilityName3);
                kuboDatas.Add(data);
            }
            sr.Close();
            return kuboDatas;
        }
    }
}
