﻿using UnityEngine;

namespace Player {
    public class PlayerMove : MonoBehaviour {
        private Vector3 cashPosition;

        Vector3 moveX = new Vector3(1.0f, 0, 0); //X軸の1マス移動分
        Vector3 moveY = new Vector3(0, 1.0f, 0); //Y軸の1マス移動分
        float step = 6f; //移動速度
        Vector3 target; //入力後の位置を保存
        Vector3 prePos; //移動できなかった場合に元に戻すため

        SpriteRenderer spriteRenderer;
        private Sprite[] walk_anime;

        [SerializeField]
        private bool debugMode = false;

        GameObject mapChipChecker;
        Map.MapChipChecker mapChipCheckerScript;
        


        //プレイヤーの方向
        public enum Player_Dir {
            down,
            left,
            up,
            right
        }

        //プレイヤーの方向を保持
        public int dirction = 0;

        //Animation
        private float player_Anime = 0.0f;

        //Animation
        private bool animation_process = false;

        //キャンバス上の十字ボタン
        private int another_Input = -1;

        // Use this for initialization
        void Start() {



            mapChipChecker = GameObject.Find("MapChipChecker");
            mapChipCheckerScript = mapChipChecker.GetComponent<Map.MapChipChecker>();


            Vector2 pos = new Vector2(673.5f, 232f);
            PlayerData.Instance.playerPos = pos;

            transform.position = PlayerData.Instance.playerPos;

            target = transform.position;

            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

            //スプライト取得
            walk_anime = Resources.LoadAll<Sprite>("Character/Player");
        }

        // Update is called once per frame
        void Update() {
            //移動中かどうかの判定
            if (transform.position == target)
            {
                SetTargetPosition();
                another_Input = -1;

                
                //中途半端にアニメーションが終わらないように
                if (animation_process == false && (int) player_Anime % 2 != 0) {
                    Walk_Anime();
                }
            } else {
                Walk_Anime(); //アニメーション
                //Move(); //移動
            }

            Move();
            if (animation_process) { Walk_Anime(); }
        }

        //キー入力とアニメーションの向き指定
        void SetTargetPosition() {
            prePos = target;
            if (Input.GetKey(KeyCode.RightArrow) || another_Input == 3) {
                if (!mapChipCheckerScript.CheckMapChipID(transform.position + moveX)) { target = transform.position + moveX; }
                dirction = (int)Player_Dir.right;   //プレイヤーの向きを変える
                animation_process = true;
                return;
            }

            if (Input.GetKey(KeyCode.LeftArrow) || another_Input == 1) {
                if (!mapChipCheckerScript.CheckMapChipID(transform.position - moveX)) { target = transform.position - moveX; }
                dirction = (int) Player_Dir.left;   //プレイヤーの向きを変える              
                animation_process = true;
                return;
            }

            if (Input.GetKey(KeyCode.UpArrow) || another_Input == 2) {
                if (!mapChipCheckerScript.CheckMapChipID(transform.position + moveY)) { target = transform.position + moveY; }
                dirction = (int) Player_Dir.up;   //プレイヤーの向きを変える  
                animation_process = true;
                return;
            }

            if (Input.GetKey(KeyCode.DownArrow) || another_Input == 0)
            {
                if (!mapChipCheckerScript.CheckMapChipID(transform.position - moveY)) { target = transform.position - moveY; }
                dirction = (int)Player_Dir.down;   //プレイヤーの向きを変える
                animation_process = true;
                return;
            }


            //デバッグ用に判定無視移動
            if(debugMode)
            {
                if (Input.GetKey(KeyCode.D) || another_Input == 3)
                {
                    target = transform.position + moveX;
                    dirction = (int)Player_Dir.right;   //プレイヤーの向きを変える
                    animation_process = true;
                    return;
                }

                if (Input.GetKey(KeyCode.A) || another_Input == 1)
                {
                    target = transform.position - moveX;
                    dirction = (int)Player_Dir.left;   //プレイヤーの向きを変える              
                    animation_process = true;
                    return;
                }

                if (Input.GetKey(KeyCode.W) || another_Input == 2)
                {
                    target = transform.position + moveY;
                    dirction = (int)Player_Dir.up;   //プレイヤーの向きを変える  
                    animation_process = true;
                    return;
                }

                if (Input.GetKey(KeyCode.S) || another_Input == 0)
                {
                    target = transform.position - moveY;
                    dirction = (int)Player_Dir.down;   //プレイヤーの向きを変える
                    animation_process = true;
                    return;
                }
            }
        }

        public void Update_Input_No(int no) {
            another_Input = no;
        }


        //移動
        void Move() {
            transform.position = Vector3.MoveTowards(transform.position, target, step * Time.deltaTime);
            PlayerData.Instance.playerPos = prePos;
        }

        //アニメーション
        void Walk_Anime() {
            player_Anime += Time.deltaTime * 10;
            if (player_Anime > 4) {
                player_Anime = 0;
            }

            spriteRenderer.sprite = walk_anime[dirction * 4 + (int) player_Anime];
            animation_process = false;
        }
    }
}
