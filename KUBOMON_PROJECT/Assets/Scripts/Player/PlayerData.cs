﻿using System;
using Systems;
using Systems.Kubomon;
using UnityEngine;

namespace Player {
    [Serializable]
    public class PlayerData : MonoBehaviour {
        //他のシーンでも一つだけ存在するための
        public static PlayerData Instance;

        //プレイヤーの座標(x,y : 0~959)
        public Vector2 playerPos;
        public PlayersKubomonStats[] Party;
        public int[] Items;


        void Awake() {
            if (null != Instance) {
                //既に存在しているなら削除
                Destroy(gameObject);
            } else {
                //存在していない場合は指定
                Instance = this;
            }
        }


        // Use this for initialization
        void Start() {
            playerPos = new Vector2(673.5f, 232f);
        }

        // Update is called once per frame
        void Update() { }
    }
}
