﻿using Player;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Map
{
    public class ShowMapName : MonoBehaviour
    {
        /// <summary>
        /// マップIDとマップ上のまちの名前と、実際に存在するモデルとなった町の名前を格納する種類の変数型
        /// </summary>
        public struct MapNameData
        {
            public string MapName;  //マップ名
            public string RealName; //実在モデルの町の名前

            public MapNameData(string mapname = "なぞのばしょ", string realname = "")
            {
                MapName = mapname;
                RealName = realname;
            }
        }


        //マップIDからマップ名と実在する(ryを取得するための辞書
        public static Dictionary<int, MapNameData> MapNameDic = new Dictionary<int, MapNameData>();

        public Text townnamepanel_MapNameText;
        public Text realnamepanel_MapNameText;

        public UI.MapNameUI MapNameUI_Town;
        public UI.MapNameUI MapNameUI_Real;

        public Vector2 prePos;
        public int beforeMapID;
        public int afterMapID;



        //マップ名表示UIと連動
        public void ShowMap(int mapID)
        {
            MapNameData data;
            string mapname;
            string realname;

            //マップIDに対応する町や道路、ダンジョン名の存在をチェック
            if (MapNameDic.ContainsKey(mapID))
            {
                data = MapNameDic[mapID];
                mapname = data.MapName;
                realname = data.RealName;
            }
            //キーに対応する値がない場合は「なぞのばしょ」を代入
            else
            {
                data = MapNameDic[0];
                mapname = data.MapName;
                realname = data.RealName;
            }

            townnamepanel_MapNameText.GetComponent<Text>().text = mapname;
            realnamepanel_MapNameText.GetComponent<Text>().text = realname;

            MapNameUI_Town.moveRotationUI = true;
            MapNameUI_Real.moveRotationUI = true;
        }



        //初回ローディング
        public void Start()
        {
            LoadMapNameData();
            Debug.Log("loadmapnamedata");
        }

        //Mapの切り替わり(まちから道路など)を監視し、変化したら変更する
        public void LateUpdate()
        {
            prePos = PlayerData.Instance.playerPos;

            //MapIDの変化(町や道路が切り替わったか)を確認し、変化していたらUIの表示を変える(y軸は-0.5fしないとずれる)
            Vector3Int idpos = new Vector3Int((int)prePos.x / 32, Mathf.FloorToInt(prePos.y - 0.5f) / 32, 0);
            afterMapID = int.Parse(MapDataLoader.GetMapID(idpos.y, idpos.x));
            if (beforeMapID != afterMapID)
            {
                beforeMapID = afterMapID;
                ShowMap(beforeMapID);
                //Debug.Log(MapNameDic[beforeMapID].MapName + MapNameDic[beforeMapID].RealName);
            }
        }


        //初回マップデータ読み込み登録
        //CSV読み込み用
        public static void LoadMapNameData()
        {
            var mysteryZone = new MapNameData("なぞのばしょ", "");
            MapNameDic.Add(0, mysteryZone);

            List<MapNameData> MapNameDatas = new List<MapNameData>();

            var csv = Resources.Load("MapNameDatas/MapNameDatas") as TextAsset;

            //StreamReader sr = new StreamReader(csv.text);
            StringReader sr = new StringReader(csv.text);

            //StreamReader sr = new StreamReader(Application.persistentDataPath + "/Objects/Texts/MapNameDatas.csv");           
            //  ↑↑↑Androidで読み込む場合はパスが異なるため使えない!!!!!

            //if (!sr.EndOfStream) sr.ReadLine();
            if (sr.Peek() > -1) sr.ReadLine();
            //while (!sr.EndOfStream)
            while (sr.Peek() > -1)
            {
                string line = sr.ReadLine();
                string[] csvDatas = line.Split(',');
                if (csvDatas.Length < 1) continue;
                int mapid = int.Parse(csvDatas[0]);
                var mapname = csvDatas[1];
                var realname = csvDatas[2];

                var data = new MapNameData(mapname, realname);
                MapNameDatas.Add(data);

                //辞書リストに順次登録
                if (MapNameDic.ContainsKey(mapid) == false)
                {
                    MapNameDic.Add(mapid, data);
                }
            }
            sr.Close();
        }
    }
    
}