﻿using Player;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Map {
    public class DrawMap : MonoBehaviour {
        private int[,] map_data = new int[960, 960];

        private float anime_time = 0.0f;

        //アニメーションするチップのデータ
        private int[] w_water_anime_tile = new int[] {166, 167, 198, 199};
        private int[] water_anime_tile = new int[] {224, 230, 236, 256, 262, 268, 274, 288, 294, 300, 306, 320, 326, 332, 338, 352, 358, 364, 370, 384, 390, 396, 402};

        Tilemap tilemap;
        Tile[] tiles;

        public Vector2 prePos;
        public Vector2 target;

        // Use this for initialization

        void Start() {
            tilemap = GetComponent<Tilemap>();
            DrawingMapInit();
        }

        // Update is called once per frame
        void LateUpdate() {
            
            if (target != prePos) {
                prePos = target;
                Draw((int) prePos.x, (int) prePos.y);
            }
            
            target = PlayerData.Instance.playerPos;
            

            anime_time += Time.deltaTime * 2;
            if (anime_time > 12) {
                anime_time = 0;
            } //水のアニメーション2回分

            Anime_Draw((int) prePos.x, (int) prePos.y);
        }


        //UnityEditor上でマップ全体を描画する命令処理
        public void DrawingMapAll()
        {
            //マップデータ受け取り
            map_data = MapDataLoader.GetMapData(transform.name);
            var tmp = GetComponent<Tilemap>();

            for (int i = 0; i < 960; i++)
            {
                for (int j = 0; j < 960; j++)
                {
                    var pos = new Vector3Int(j, i, 0);
                    tmp.SetTile(pos, MapDataLoader.tile[map_data[j, i]]);
                }
            }
        }

        //UnityEditor上の敷き詰めたタイルマップを全消し
        public void ClearTile()
        {
            var tmp = GetComponent<Tilemap>();
            tmp.ClearAllTiles();
        }


        //マップに必要なデータ類を受け取る
        public void DrawingMapInit() {
            //マップデータ受け取り
            map_data = MapDataLoader.GetMapData(transform.name);

            prePos = PlayerData.Instance.playerPos;
            Draw((int)prePos.x, (int)prePos.y);
        }


        //マップを描画する
        public void Draw(int draw_x, int draw_y) {
            //tilemap.ClearAllTiles();  //タイルマップを削除

            for (int i = draw_y - 10; i < draw_y + 10; i++) {
                for (int j = draw_x - 16; j < draw_x + 17; j++) {
                    if (i < 0) {
                        i = 0;
                    }

                    if (i > 960) {
                        i = 960;
                    }

                    if (j < 0) {
                        j = 0;
                    }

                    if (j > 960) {
                        j = 960;
                    }

                    var pos = new Vector3Int(j, i, 0);
                    //Tileが透明のやつは描画を飛ばしつつマップチップを描画する
                    if (MapDataLoader.tile[map_data[j, i]].name != "image_0"){ tilemap.SetTile(pos, MapDataLoader.tile[map_data[j, i]]); }
                }
            }
        }

        //アニメーションするマップのみ更新
        void Anime_Draw(int draw_x, int draw_y) {
            for (int i = draw_y - 10; i < draw_y + 10; i++) {
                for (int j = draw_x - 16; j < draw_x + 17; j++) {
                    if (i < 0) {
                        i = 0;
                    }

                    if (i > 960) {
                        i = 960;
                    }

                    if (j < 0) {
                        j = 0;
                    }

                    if (j > 960) {
                        j = 960;
                    }

                    //水のアニメーションのタイル確認
                    for (int ani = 0; ani < water_anime_tile.Length; ani++) {
                        if (map_data[j, i] == water_anime_tile[ani]) {
                            var pos = new Vector3Int(j, i, 0);
                            tilemap.SetTile(pos, MapDataLoader.tile[map_data[j, i] + (int) anime_time % 6]);
                        }
                    }

                    //水の2倍サイズアニメーションのタイル確認
                    for (int ani = 0; ani < w_water_anime_tile.Length; ani++) {
                        if (map_data[j, i] == w_water_anime_tile[ani]) {
                            var pos = new Vector3Int(j, i, 0);
                            tilemap.SetTile(pos, MapDataLoader.tile[map_data[j, i] + (int) anime_time % 6 * 2]);
                        }
                    }
                }
            }
        }

        //x,yの座標のマップチップIDを返す
        public int GetMapChipID(Vector3Int pos)
        {
            int mapId = map_data[pos.x, pos.y];
            return mapId;
        }

    }
}
