﻿using System.IO;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Map {
    public static class MapDataLoader {
        //タイルの数
        public const int number_of_tiles = 2048;


        //ワールド全体のmapIDを取得
        private static string[,] mapID_datas = new string[30, 30];


        //private Sprite[] sprite;
        public static Tile[] tile = new Tile[number_of_tiles];


        static GameObject Deep;
        static GameObject Under;
        static GameObject Front;

        static DrawMap drawmap_d;
        static DrawMap drawmap_du;
        static DrawMap drawmap_f;

        // Use this for initialization
        static MapDataLoader() {
            //ワールド全体を構成するIDデータを取得
            mapID_datas = LoadMapData("Map_ID", 30);

            //タイルを順番にロードする、LoadAllだとグチャる
            for (int i = 0; i < number_of_tiles; i++) {
                string f = "Tile/image_" + i.ToString();
                tile[i] = Resources.Load<Tile>(f);
            }



        }

        //マップチップ(タイルを渡す)
        public static Tile[] GetTile() {
            return tile;
        }

        //マップチップの数(タイルの数を渡す)
        public static int GetNumberOfTiles() {
            return number_of_tiles;
        }

        //マップチップデータ(マップデータを渡す)
        public static int[,] GetMapData(string Layer) {
            //Debug.Log(Layer);
            int[,] mapdata = new int[960, 960];

            for (int y = 0; y < 30; y++) {
                for (int x = 0; x < 30; x++) {
                    string[,] map_tiles = new string[32, 32];
                    switch (Layer) {
                        case "Deep":
                            map_tiles = LoadMapData("Map_" + mapID_datas[y, x] + "d", 32);
                            break;
                        case "Under":
                            map_tiles = LoadMapData("Map_" + mapID_datas[y, x] + "du", 32);
                            break;
                        case "Front":
                            map_tiles = LoadMapData("Map_" + mapID_datas[y, x] + "f", 32);
                            break;
                        default:
                            break;
                    }

                    for (int write_y = 0; write_y < 32; write_y++) {
                        for (int write_x = 0; write_x < 32; write_x++) {
                            mapdata[y * 32 + write_y, x * 32 + write_x] = int.Parse(map_tiles[write_y, write_x]);
                        }
                    }
                }
            }
            return mapdata;
        }


        //Map_Data内のファイル名を入れるとcsvをList<string[]>で返す
        public static string[,] LoadMapData(string fileName, int size)
        {
            //一時的なデータ入れ
            string[,] datas = new string[size, size];

            // Resourcesのcsvフォルダ内のcsvファイルをTextAssetとして取得
            var csvFile = Resources.Load("Map_Data/" + fileName) as TextAsset;

            if (csvFile == null) {
                csvFile = Resources.Load("Map_Data/Map_0_0f") as TextAsset;
            }

            // csvファイルの内容をStringReaderに変換
            var reader = new StringReader(csvFile.text);

            int j = size - 1;
            // csvファイルの内容を一行ずつ末尾まで取得しリストを作成
            while (reader.Peek() > -1) {
                // 一行読み込む
                var lineData = reader.ReadLine();
                // カンマ(,)区切りのデータを文字列の配列に変換
                var address = lineData.Split(',');

                //配列に追加
                for (int i = 0; i < size; i++) {
                    datas[i, j] = address[i];
                }

                j--;
                // 末尾まで繰り返し...
            }

            return datas;
        }


        //マップIDを返す
        public static string GetMapID(int x, int y)
        {
            string ID;
            if (mapID_datas[y, x] != "0"){ ID = mapID_datas[y, x].Substring(0, mapID_datas[y, x].IndexOf("_")); }
            else { ID = "0"; }  //マップID自体が存在しない地域には「なぞのばしょ」のIDを適応
            return ID;
        }
    }
}
