﻿/*
using System;
using System.Linq;
using Systems;
using Systems.Kubomon;
using Animation;
using Battle;
using Map;
using UnityEngine;
using UnityEngine.UI;

namespace test {
    /// <summary>
    /// バトル画面に移動するためのやつ
    /// </summary>
    public class TestEncounter : MonoBehaviour {
        private bool isBattle;
        [SerializeField] private AnimationBase VSKubomon;
        [SerializeField] private AnimationBase VSTrainer1;
        [SerializeField] private AnimationBase VSTrainer2;
        [SerializeField] private BattleManager battleManager;
        [SerializeField] private Sprite playerSprite;
        [SerializeField] private Sprite enemySprite;
        [SerializeField] private KubomonStats[] players;
        [SerializeField] private KubomonStats kubo;
        [SerializeField] private KubomonStats[] enemy1;
        [SerializeField] private KubomonStats[] enemy2;
        [SerializeField] private string trainerName;
        [SerializeField] private string enemy1Type;
        [SerializeField] private string enemy1Name;
        [SerializeField] private string enemy2Type;
        [SerializeField] private string enemy2Name;

        private State state;

        enum State {
            Kubo, Single, Double
        }

        private void Update() {
            if (isBattle) return;
            if (Input.GetKeyDown(KeyCode.Z)) {
                isBattle = true;
                state = State.Kubo;
                AnimationController.Instance.RegisterAnimation(VSKubomon);
            }

            if (Input.GetKeyDown(KeyCode.X)) {
                isBattle = true;
                state = State.Single;
                AnimationController.Instance.RegisterAnimation(VSTrainer1);
            }

            if (Input.GetKeyDown(KeyCode.C)) {
                isBattle = true;
                state = State.Double;
                AnimationController.Instance.RegisterAnimation(VSTrainer2);
            }

            if (isBattle) {
                BattleStart();
            }
        }

        public void BattleStart() {
            switch (state) {
                case State.Kubo:
                    battleManager.KuboBattleStart(playerSprite, players, kubo, trainerName);
                    break;
                case State.Single:
                    battleManager.SingleBattleStart(playerSprite, enemySprite, players, enemy1, trainerName, enemy1Type, enemy1Name);
                    break;
                case State.Double:
                    Debug.LogError("まだ");
                    //battleManager.DoubleBattleStart(players, new KubomonStatus[2][] {enemy1, enemy2});
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Reset() {
            isBattle = false;
        }
    }
}
*/
