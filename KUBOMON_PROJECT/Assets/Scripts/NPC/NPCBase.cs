﻿using System;
using UnityEngine;

namespace NPC {
    public abstract class NPCBase : MonoBehaviour{
        public MessageInfo[] Message;
        [SerializeField] protected EWalkType WalkType;

        private void Update() {
            //TODO: ポーズやバトル中とかの時以外の条件分岐
            Move();
        }

        private void Move() {
            switch (WalkType) {
                case EWalkType.Stop:
                    break;
                case EWalkType.Random:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
