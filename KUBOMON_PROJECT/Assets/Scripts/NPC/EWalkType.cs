﻿namespace NPC {
    /// <summary>
    /// 歩き方。この数だけ、NPCBaseのWalkに書く量は増える
    /// </summary>
    public enum EWalkType {
        Stop, Random
    }
}
