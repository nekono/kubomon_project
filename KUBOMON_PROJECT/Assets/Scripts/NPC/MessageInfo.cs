﻿using System;

namespace NPC {
    [Serializable]
    public struct MessageInfo {
        public EMessageType Type;
        public string Message;
    }
}
