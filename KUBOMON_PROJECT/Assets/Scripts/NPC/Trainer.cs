﻿using Systems;
using Systems.Kubomon;
using UnityEngine;

namespace NPC {
    public class Trainer : NPCBase {
        [SerializeField] private string trainerType;
        [SerializeField] private string trainerName;
        [SerializeField] private int money;
        [SerializeField] private int spriteNumber;
        [SerializeField] private int heldItemNumber;
        
        public string TrainerType => trainerType;
        public string TrainerName => trainerName;
        public TrainersKubomonStats[] Party;
        public int Money => money;
        public int SpriteNumber => spriteNumber;
        public int HeldItemNumber => heldItemNumber;
    }
}
