using System;
using Systems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;

namespace Animation {
    /// <summary>
    /// アニメーションを管理するクラス
    /// 全部のカットインや技のアニメーションをInstantiateして再生させる
    /// </summary>
    public class AnimationController : SingletonMonoBehaviour<AnimationController> {
        private Tuple<AnimationBase, bool> currentAnimation;
        private List<Tuple<AnimationBase, bool, PlayableAsset>> nextAnimations = new List<Tuple<AnimationBase, bool, PlayableAsset>>();
        
        /// <summary>
        /// 次に再生するアニメーションの登録をする
        /// </summary>
        public void RegisterAnimation(AnimationBase anim, bool clone = true, PlayableAsset asset = null) {
            if (currentAnimation == null && nextAnimations.Count <= 0) {
                currentAnimation = clone ? new Tuple<AnimationBase, bool>(Instantiate(anim), true) : new Tuple<AnimationBase, bool>(anim, false);

                currentAnimation.Item1.PlayAnim(asset);
            } else {
                nextAnimations.Add(new Tuple<AnimationBase, bool, PlayableAsset>(anim, clone, asset));
            }
        }

        /// <summary>
        /// アニメーションを強制再生する
        /// </summary>
        public void ForcePlayAnimation(AnimationBase anim, bool clone = true, PlayableAsset asset = null) {
            if (currentAnimation != null && currentAnimation.Item2) Destroy(currentAnimation.Item1.gameObject);
            else currentAnimation.Item1.Stop();
            currentAnimation = clone ? new Tuple<AnimationBase, bool>(Instantiate(anim), true) : new Tuple<AnimationBase, bool>(anim, false);
            currentAnimation.Item1.PlayAnim(asset);
        }

        /// <summary>
        /// 次のアニメーションを再生する
        /// </summary>
        private void PlayNext() {
            if (nextAnimations.Count <= 0) return;
            ForcePlayAnimation(nextAnimations[0].Item1, nextAnimations[0].Item2, nextAnimations[0].Item3);
            nextAnimations.RemoveAt(0);
        }

        private void Update() {
            if (currentAnimation != null) {
                if (currentAnimation.Item1.IsPlaying) return;
                PlayNext();
            } else if (nextAnimations.Count > 0) {
                PlayNext();
            }
        }
    }
}
