using System;
using UniRx;
using UnityEngine;
using UnityEngine.Playables;

namespace Animation {
    /// <summary>
    /// アニメーションを再生する親クラス
    /// AnimStart等はAnimationControllerから呼ばれる
    /// </summary>
    public abstract class AnimationBase : MonoBehaviour {
        public bool IsPlaying { get; protected set; }
        protected PlayableDirector director;
        private Subject<Unit> endStream = new Subject<Unit>();
        public Subject<Unit> EndStream => endStream;

        private void Awake() {
            director = GetComponent<PlayableDirector>();
        }

        public virtual void PlayAnim(PlayableAsset asset = null) {
            if (asset != null) {
                director.playableAsset = asset;
            }
            director.Play();
            IsPlaying = true;
        }

        public void Stop() {
            director.Stop();
        }

        protected virtual void EndCheck() {
            if (director.state != PlayState.Playing) {
                IsPlaying = false;
                endStream.OnNext(Unit.Default);
            }
        }

        private void Update() {
            if (IsPlaying) {
                EndCheck();
            }
        }
    }
}
