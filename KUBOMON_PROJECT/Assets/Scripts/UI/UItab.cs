﻿using UnityEngine;

namespace UI {
    public class UItab : MonoBehaviour {
        [SerializeField, Range(0, 10)]
        float time = 0.4f;
        private float startTime;

        //UITabが有効か無効か
        bool onUIenable = false;
        //UI稼働中か否か
        bool moving = false;
        //UIが最初からいたポジション
        Vector2 intialPosition;
        //向かうべき場所へ
        Vector2 purposePosition;

        [SerializeField]
        UItabManager uItabManager;

        // Use this for initialization
        void Start () {
            intialPosition = transform.localPosition;
            purposePosition = new Vector2(0, 0);

            //Debug.Log(intialPosition);
            //Debug.Log(purposePosition);

            //uItabManager = GetComponent<UItabManager>();  //インスペクター上でアタッチ
        }
	
        // Update is called once per frame
        void Update () {
            if (moving)
            {
                var diff = Time.timeSinceLevelLoad - startTime;
                if(diff > time)
                {
                    transform.localPosition = purposePosition;
                    moving = false;
                }
                var rate = diff / time;

                //UIを有効/無効で切り替えた時に移動させる。
                if (onUIenable)
                {
                    transform.localPosition = Vector2.Lerp(intialPosition, purposePosition, rate);
                    transform.SetSiblingIndex(6);
                }
                else
                {
                    transform.localPosition = Vector2.Lerp(purposePosition, intialPosition, rate);
                
                }


            }



        }

        public void PointerDown()
        {
            if (!moving)
            {
                onUIenable = !onUIenable;
                startTime = Time.timeSinceLevelLoad;
                moving = true;

                //開くときにUItabManagerに開いたことを通知
                if (onUIenable)
                {
                    //Debug.Log(uItabManager + ":" + gameObject.name);
                    Debug.Log(Application.persistentDataPath + "/Objects/Texts/MapNameDatas.csv");
                    uItabManager.SetEnableUI(gameObject.name);
                }

                //閉じるときにUItabManagerに閉じたことを通知
                if (!onUIenable)
                {
                    uItabManager.UnenbleUI(gameObject.name);
                }
            }

        }

        public void UIoff()
        {
            if (!moving)
            {
                onUIenable = !onUIenable;
                startTime = Time.timeSinceLevelLoad;
                moving = true;
            }

        }
    }
}
