﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MapNameUI : MonoBehaviour
    {
        [SerializeField]
        public bool moveRotationUI = false;     //UIの回転が有効か否か
        [SerializeField]
        private int stopMoveTime = 100;         //回転して、いったん止まる時間の長さの指定
        [SerializeField]
        private float rotateSpeed = 20f;        //回転速度
        [SerializeField]
        private float rotateAngle = 7.0f;       //回転する角度

        RectTransform rectTransform;            //RectTransformを入れる
        private int stopMoveTimeValue;          //回転して、いったん止まる時間の長さ


        // Use this for initialization
        void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            stopMoveTimeValue = stopMoveTime;
        }

        // Update is called once per frame
        void Update()
        {

            //表示する方向に回転する
            if (rectTransform.rotation.z < Quaternion.Euler(0, 0, rotateAngle).z && moveRotationUI && stopMoveTimeValue > 0)
            {
                float step = rotateSpeed * Time.deltaTime;
                rectTransform.rotation = Quaternion.RotateTowards(rectTransform.rotation, Quaternion.Euler(0, 0, rotateAngle), step);
            }

            //一旦停止して表示を続ける
            if(rectTransform.rotation.z >= Quaternion.Euler(0, 0, rotateAngle).z && stopMoveTimeValue > 0)
            {
                stopMoveTimeValue = stopMoveTimeValue - 1;
                moveRotationUI = false;
            }

            //一旦停止した後元の位置へ戻っていく
            if(stopMoveTimeValue <= 0 && moveRotationUI == false)
            {
                float step = rotateSpeed * Time.deltaTime;
                rectTransform.rotation = Quaternion.RotateTowards(rectTransform.rotation, Quaternion.Euler(0, 0, 0), step);
            }

            //状態をリセット?
            if(stopMoveTimeValue != stopMoveTime && moveRotationUI)
            {
                stopMoveTimeValue = stopMoveTime;
            }


        }

        //UIを回転移動させ、名前を見せた後元に戻る動き
        public void MoveRotationUI()
        {

        }
    }
}