﻿using UnityEngine;

namespace UI {
    public class UItabManager : MonoBehaviour
    {

        //UIが有効か否か(左から{Dex,Kubomon,Bag,Trainer,Report,Config})
        public bool[] onUI = new bool[] { false, false, false, false, false, false };

        //UIを入れておく
        public GameObject[] UIs;
        public UItab[] UIscripts;


        // Use this for initialization
        void Start()
        {

            //もっといい方法があるとは思うがわからないので一つずつFind GetComponent
            //UIs[0] = GameObject.Find("Canvas/UI_Tab_Dex");
            //UIs[1] = GameObject.Find("UI_Tab_Kubomon");
            //UIs[2] = GameObject.Find("UI_Tab_Bag");
            //UIs[3] = GameObject.Find("UI_Tab_Trainer");
            //UIs[4] = GameObject.Find("UI_Tab_Report");
            //UIs[5] = GameObject.Find("UI_Tab_Config");

            //これで取得しようとしたができなかったためインスペクター上でアタッチ
            //UIscripts[0] = UIs[0].GetComponent<UItab>();
            //UIscripts[1] = UIs[1].GetComponent<UItab>();
            //UIscripts[2] = UIs[2].GetComponent<UItab>();
            //UIscripts[3] = UIs[3].GetComponent<UItab>();
            //UIscripts[4] = UIs[4].GetComponent<UItab>();
            //UIscripts[5] = UIs[5].GetComponent<UItab>();

        }

        // Update is called once per frame
        void Update()
        {

        }

        //UIの名前から番号を返す
        private int GetUInumber(string name)
        {
            int no = -1;

            //Unityもプログラムの書き方も分からな過ぎてまさかの手法でUIのナンバーを取得
            switch (name)
            {
                case "UI_Tab_Dex":
                    no = 0;
                    break;
                case "UI_Tab_Kubomon":
                    no = 1;
                    break;
                case "UI_Tab_Bag":
                    no = 2;
                    break;
                case "UI_Tab_Trainer":
                    no = 3;
                    break;
                case "UI_Tab_Report":
                    no = 4;
                    break;
                case "UI_Tab_Config":
                    no = 5;
                    break;

                default:
                    break;
            }
            return no;
        }


        public void UnenbleUI(string uiname)
        {
            onUI[GetUInumber(uiname)] = false;
        }

        public void SetEnableUI(string uiname)
        {
            int no = -1;
            no = GetUInumber(uiname);

            for (int i = 0; i < onUI.Length; i++)
            {
                if (onUI[i])
                {
                    UIscripts[i].UIoff();
                    onUI[i] = false;
                }
            }
            onUI[no] = true;
        }
    }
}



