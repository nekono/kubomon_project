﻿using Player;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI {

    public class ButtonHandler : MonoBehaviour {
        private PlayerMove playerMove;

        bool push = false;

        int pushTime = 0;

        // Use this for initialization
        void Start() {
            playerMove = GameObject.FindWithTag("Player").GetComponent<PlayerMove>();
        }

        // Update is called once per frame
        void Update() {
            if (push) {
                pushTime++;
                if (pushTime > 9) {
                    Button();
                    pushTime = 0;
                }
            }
        }

        public void PointerDown() {
            push = true;
            pushTime = 8;
        }

        public void PointerUp() {
            push = false;
        }

        private void Button() {
            int no = -1;
            switch (transform.name) {
                case ("Button_Left"):
                    no = (int)PlayerMove.Player_Dir.left;
                    break;
                case ("Button_Up"):
                    no = (int)PlayerMove.Player_Dir.up;
                    break;
                case ("Button_Right"):
                    no = (int) PlayerMove.Player_Dir.right;
                    break;
                case ("Button_Down"):
                    no = (int)PlayerMove.Player_Dir.down;
                    break;
            }

            playerMove.Update_Input_No(no);
        }
    }
}
